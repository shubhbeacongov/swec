'use strict';
/**
 * @ngdoc overview
 * @name swecApp
 * @description
 * # swecApp
 *
 * Main module of the application.
 */
angular.module('swecApp', ['ngRoute', 'ngMaterial', 'ngMessages', 'material.svgAssetsCache', 'ngIdle'])
  // AJAX status
  .factory('httpInterceptor', ['$q', '$rootScope',
    function ($q, $rootScope) {
      var loadingCount = 0;
      return {
        request: function (config) {
          if (++loadingCount === 1) $rootScope.$broadcast('http:running');
          return config || $q.when(config);
        },
        response: function (response) {
          if (--loadingCount === 0) $rootScope.$broadcast('http:finished');
          return response || $q.when(response);
        },
        responseError: function (response) {
          if (--loadingCount === 0) $rootScope.$broadcast('http:finished');
          return $q.reject(response);
        }
      };
    }
  ])
  // Routing
  .config(function ($routeProvider, $httpProvider, IdleProvider, KeepaliveProvider, $locationProvider) {
    $routeProvider.when('/', {
      templateUrl: 'labnbill/views/userLogin.html',
      controller: 'userLoginCtrl',
      controllerAs: 'userLogin'
    }).when('/patientLogin', {
      templateUrl: 'labnbill/views/patientLogin.html',
      controller: 'patientLoginCtrl',
      controllerAs: 'patientLogin'
    }).when('/main', {
      templateUrl: 'labnbill/views/main.html',
      controller: 'MainCtrl',
      controllerAs: 'main'
    }).when('/about', {
      templateUrl: 'labnbill/views/about.html',
      controller: 'AboutCtrl',
      controllerAs: 'about'
    }).when('/test', {
      templateUrl: 'labnbill/views/test.html',
      controller: 'testCtrl',
      controllerAs: 'test'
    }).when('/bill', {
      templateUrl: 'labnbill/views/bill.html',
      controller: 'billCtrl',
      controllerAs: 'bill'
    }).when('/preview', {
      templateUrl: 'labnbill/views/preview.html',
      controller: 'previewCtrl',
      controllerAs: 'preview'
    }).when('/external', {
      templateUrl: 'external/views/external.html',
      controller: 'externalCtrl',
      controllerAs: 'external'
    }).otherwise({
      redirectTo: '/'
    });
    // Ideal time management
    IdleProvider.idle(1200); //(10*60); // 10 minutes idle
    IdleProvider.timeout(6000); //(30); // after 30 seconds idle, time the user out
    $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    $httpProvider.interceptors.push('httpInterceptor');
    // use the HTML5 History API
    $locationProvider.html5Mode(true);
  }).run(function ($rootScope, Idle, $location) {
    //screenResized();
    $rootScope.refreshData = true;
    $rootScope.testList = [];
    $rootScope.$on('IdleTimeout', function () {
      $rootScope.redirect("");
      // end their session and redirect to login
    });
    $rootScope.$on('IdleStart', function () {
      $rootScope.redirect("patientLogin");
    });
    Idle.watch();
    $rootScope.waitingPopup = false;
    // For Alerts
    $rootScope.alert = function (pMsg) {
      $rootScope.alertMsg = pMsg;
      $rootScope.alertTimer = $timeout(function () {
        $rootScope.alertMsg = "";
      }, 5000);
    }
    $rootScope.alertDismiss = function () {
      $timeout.cancel($rootScope.alertTimer);
      $rootScope.alertMsg = "";
    }
    // Navigating Function
    $rootScope.redirect = function (pAddress) {
      $location.url(pAddress);
    }
    // Age format correction
    $rootScope.correctAge = function (pAge) {
      if (pAge == null) {
        return "";
      }
      var vAge = pAge.replace("mons", "months");
      return vAge;
    }
  });
// Global URL of server
function getUrlIP() {
  return "http://192.168.185.154:8080/";
};