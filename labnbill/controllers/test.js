'use strict';
/**
 * @ngdoc function
 * @name kiosklabApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the kiosklabApp
 */
angular.module('swecApp').controller('testCtrl', function ($scope, $http, $rootScope, $timeout) {
  $scope.disablled = true;
  // Alphabets Filter data
  var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $scope.MyalphabetsList = str.match(/.{1,3}/g);
  // Number Filter Data
  var num = ['1', '2', '3', '4', '5', '6', '7', '8', '90'];
  $scope.MynumbersList = num;
  $scope.testRepList = [];
  $scope.ajaxCallFinished = true;
  $scope.confirmClicked = true;
  // Init for the first load
  $scope.firstTimeInit = function () {
    // Fetching Procedure Data from server
    $rootScope.departments = [];
    $rootScope.procRepList = [];
    $rootScope.buttonColor = [];
    $rootScope.toPreview = [];
    $rootScope.radiologyDepartments = [];
    $rootScope.labIndexes = [];
    $rootScope.MyLab = [];
    $rootScope.radilogyUnitIndex = {
      deptIndex: null,
      unitIndex: null
    };
    $http.get("./labnbill/data/color.json").then(function (response) {
      $rootScope.buttonColor = response.data;
    });
    $http.post(getUrlIP() + "Spring/restServices/departProcedure", {
      "uhId": $rootScope.loggedInpatientDetail.uhId,
      "mobileNumber": $rootScope.loggedInpatientDetail.mobile
    }).then(function (response) {
      $rootScope.departments = response.data;
      var procRepListRadio = [];
      for (var i = 0; i < $rootScope.departments.length; i++) {
        var colorCount = 0;
        $rootScope.departments[i]["sel"] = false;
        for (var j = 0; j < $rootScope.departments[i].unitJ.length; j++) {
          $rootScope.departments[i].unitJ[j]["color"] = $rootScope.buttonColor[colorCount].color;
          $rootScope.departments[i].unitJ[j]["sel"] = false;
          if (colorCount == $rootScope.buttonColor.length - 1) {
            colorCount = 0;
          } else {
            colorCount++;
          }
          for (var t = 0; t < $rootScope.buttonColor.length; t++) {
            if ($rootScope.departments[i].unitJ[j].color == $rootScope.buttonColor[t].color) {
              $rootScope.departments[i].unitJ[j]["bordercolor"] = $rootScope.buttonColor[t].bordercolor;
              $rootScope.departments[i].unitJ[j]["spancolor"] = $rootScope.buttonColor[t].spancolor;
              $rootScope.departments[i].unitJ[j]["labButtonColor"] = $rootScope.buttonColor[t].labButtonColor;
              break;
            }
          }
          for (var k = 0; k < $rootScope.departments[i].unitJ[j].clinicJ.length; k++) {
            $rootScope.departments[i].unitJ[j].clinicJ[k]["sel"] = false;
            procRepListRadio.push({
              "deptIndex": i,
              "unitIndex": j,
              "clinicIndex": k
            });
          }
        }
      }
      //--Radiology Data----------------------------------------
      $http.get("./labnbill/data/radiology.json").then(function (response) {
        $rootScope.radiologyDepartments = response.data;
        $rootScope.radiologyAreaList = [];
        var areaListCount = 0;
        var radiologyDepartmentData = {
          "id": 500,
          "name": "Imaging",
          "sel": false,
          "unitJ": [{
            "testId": 501,
            "unitName": "Radiology",
            "sel": false,
            "radiologyFlag": true,
            "color": $rootScope.buttonColor[0].color,
            "bordercolor": $rootScope.buttonColor[0].bordercolor,
            "spancolor": $rootScope.buttonColor[0].spancolor,
            "labButtonColor": $rootScope.buttonColor[0].labButtonColor,
            "clinicJ": []
          }]
        };
        $rootScope.radilogyUnitIndex.deptIndex = $rootScope.departments.length;
        $rootScope.radilogyUnitIndex.unitIndex = 0;
        for (var i = 0; i < $rootScope.radiologyDepartments.testj.length; i++) {
          radiologyDepartmentData.unitJ[0].clinicJ.push({
            id: $rootScope.radiologyDepartments.testj[i].testId,
            name: $rootScope.radiologyDepartments.testj[i].testName,
            sel: false,
            radiologyDepartmentsIndex: i,
            isOrdered: false
          });
          procRepListRadio.push({
            "deptIndex": $rootScope.radilogyUnitIndex.deptIndex,
            "unitIndex": $rootScope.radilogyUnitIndex.unitIndex,
            "clinicIndex": i
          });
          for (var j = 0; j < $rootScope.radiologyDepartments.testj[i].imageAreaJ.length; j++) {
            $rootScope.radiologyAreaList.push({
              "T": i,
              "area": j,
              "sel": false,
              "selfIndex": (areaListCount++),
              "details": {
                "medium": null,
                "type": null,
                "view": null,
                "position": null
              },
              selectedDetails: []
            });
            $rootScope.radiologyOrderedDataMaping($rootScope.radiologyAreaList.length - 1, procRepListRadio[procRepListRadio.length - 1], radiologyDepartmentData);
          }
        }
        $rootScope.departments.push(radiologyDepartmentData);
        $rootScope.procRepList = procRepListRadio;
        $rootScope.waitingPopup = false;
      });
      //--Radiology Data----------END------------------------------
    }, function (error) {
      $rootScope.waitingPopup = false;
    });
    $http.get(getUrlIP() + "Spring/restServices/radioTestToDefRoomAvlDate/" + $rootScope.loggedInpatientDetail.uhId).then(function (response) {
      if (response.data.errorCode !== undefined) {
        if (response.data.errorCode == "404") {
          $rootScope.radiologyOrderData = [];
        }
      } else {
        $rootScope.radiologyOrderData = response.data;
        for (var i = 0; i < $rootScope.radiologyOrderData.length; i++) {
          $rootScope.radiologyOrderData[i]["ordered"] = true;
          for (var j = 0; j < $rootScope.radiologyOrderData[i].rooms.length; j++) {
            $rootScope.radiologyOrderData[i].rooms[j]["defaultDate"] = $rootScope.radiologyOrderData[i].rooms[j].date[0];
            for (var k = 0; k < $rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea.length; k++) {
              if ($rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea[k].constructor === Array) {
                $rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea[k] = $rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea[k][0];
              }
              $rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea[k]["active"] = true;
            }
          }
        }
      }
    });
    $rootScope.radiologyOrderedDataMaping = function (pIndex, pDeptData, pDept) {
      for (var i = 0; i < $rootScope.radiologyOrderData.length; i++) {
        if ($rootScope.radiologyOrderData[i].imagingType == $rootScope.radiologyDepartments.testj[$rootScope.radiologyAreaList[pIndex].T].testName) {
          for (var iRoom = 0; iRoom < $rootScope.radiologyOrderData[i].rooms.length; iRoom++) {
            for (var iProcedure = 0; iProcedure < $rootScope.radiologyOrderData[i].rooms[iRoom].procedures.length; iProcedure++) {
              for (var iArea = 0; iArea < $rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea.length; iArea++) {
                if ($rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].id == $rootScope.radiologyDepartments.testj[$rootScope.radiologyAreaList[pIndex].T].imageAreaJ[$rootScope.radiologyAreaList[pIndex].area].id) {
                  $rootScope.radiologyAreaList[pIndex].sel = true;
                  for (var iAreaDet = 0; iAreaDet < $rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].imageTypeJ.length; iAreaDet++) {
                    $rootScope.radiologyAreaList[pIndex].selectedDetails.push({
                      "medium": ($rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].imageMediumJ[iAreaDet] != null ? $rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].imageMediumJ[iAreaDet] : null),
                      "type": ($rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].imageTypeJ[iAreaDet] != null ? $rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].imageTypeJ[iAreaDet] : null),
                      "view": ($rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].imageViewJ[iAreaDet] != null ? $rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].imageViewJ[iAreaDet] : null),
                      "position": ($rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].imagePositionJ[iAreaDet] != null ? $rootScope.radiologyOrderData[i].rooms[iRoom].procedures[iProcedure].imagingArea[iArea].imagePositionJ[iAreaDet] : null)
                    });
                    pDept.unitJ[pDeptData.unitIndex].clinicJ[pDeptData.clinicIndex].isOrdered = true;
                    pDept.unitJ[pDeptData.unitIndex].clinicJ[pDeptData.clinicIndex].sel = true;
                    pDept.unitJ[pDeptData.unitIndex].sel = true;
                    pDept.sel = true;
                    if ($rootScope.radiologyFilterOnTestIndexOf(i) < 0) {
                      $rootScope.radiologyFilterOnTest.push({
                        "tIndex": i,
                        "sel": true
                      });
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    $rootScope.radiologyFilterOnTest = [];
    //----Fetching Test Ordered Data from server-------
    $rootScope.testList = [];
    $rootScope.testIndexList = [];
    $http.get(getUrlIP() + "Spring/restServices/testToClCntr/" + $rootScope.loggedInpatientDetail.uhId).then(function (response) {
      if (response.data.errorCode !== undefined) {
        if (response.data.errorCode == "404") {
          $rootScope.alreadyOrdered = false;
        }
      } else {
        $rootScope.testList = response.data;
        for (var i = 0; i < $rootScope.testList.length; i++) {
          for (var j = 0; j < $rootScope.testList[i].rooms.length; j++) {
            $rootScope.testList[i].rooms[j]["defaultDate"] = $rootScope.testList[i].rooms[j].date[0];
            for (var k = 0; k < $rootScope.testList[i].rooms[j].test.length; k++) {
              $rootScope.testIndexList.push({
                "collecCntrNameIndex": i,
                "roomIndex": j,
                "testIndex": k
              });
              $rootScope.testList[i].rooms[j].test[k]["active"] = true;
            }
          }
        }
        $rootScope.alreadyOrdered = true;
      }
    });
    //---- End Fetching Test Ordered Data from server ----
    if (!$rootScope.alreadyOrdered) {
      $http.get(getUrlIP() + "Spring/restServices/labAvailableDates").then(function (response) {
        $rootScope.MyLab = response.data;
        for (var i = 0; i < $rootScope.MyLab.length; i++) {
          $rootScope.MyLab[i]["sel"] = false;
          for (var j = 0; j < $rootScope.MyLab[i].unitJ.length; j++) {
            $rootScope.MyLab[i].unitJ[j]["sel"] = false;
            for (var k = 0; k < $rootScope.MyLab[i].unitJ[j].clinicJ.length; k++) {
              $rootScope.MyLab[i].unitJ[j].clinicJ[k]["sel"] = false;
              $rootScope.MyLab[i].unitJ[j].clinicJ[k]["defaultRoom"] = 0;
              $rootScope.labIndexes.push({
                "labDeptIndex": i,
                "labUnitIndex": j,
                "labClinicIndex": k
              });
              for (var m = 0; m < $rootScope.MyLab[i].unitJ[j].clinicJ[k].roomJ.length; m++) {
                $rootScope.MyLab[i].unitJ[j].clinicJ[k].roomJ[m]["sel"] = false;
                $rootScope.MyLab[i].unitJ[j].clinicJ[k].roomJ[m]["defaultDate"] = $rootScope.MyLab[i].unitJ[j].clinicJ[k].roomJ[m].appointmentDate[0];
              }
            }
          }
        }
      });
    }
  };
  // Called Initiate the UI
  $scope.setupInit = function () {
    if ($rootScope.refreshData) {
      $scope.firstTimeInit();
      $rootScope.refreshData = false;
    }
  };
  // Called when controller loads
  $scope.setupInit();
  // Click of Lab
  $rootScope.lab_Click = function (lData) {
    $rootScope.MyLab[lData.labDeptIndex].unitJ[lData.labUnitIndex].clinicJ[lData.labClinicIndex].sel = !$rootScope.MyLab[lData.labDeptIndex].unitJ[lData.labUnitIndex].clinicJ[lData.labClinicIndex].sel;
    if ($rootScope.MyLab[lData.labDeptIndex].unitJ[lData.labUnitIndex].clinicJ.findIndex(function (pD) {
        return pD.sel == true
      }) > -1) {
      $rootScope.MyLab[lData.labDeptIndex].unitJ[lData.labUnitIndex].sel = true;
    } else {
      $rootScope.MyLab[lData.labDeptIndex].unitJ[lData.labUnitIndex].sel = false;
    }
    if ($rootScope.MyLab[lData.labDeptIndex].unitJ.findIndex(function (pD) {
        return pD.sel == true
      }) > -1) {
      $rootScope.MyLab[lData.labDeptIndex].sel = true;
    } else {
      $rootScope.MyLab[lData.labDeptIndex].sel = false;
    }
  };
  // Finds the index of object in radiologyFilterOnTest on the bases of inputs given
  $rootScope.radiologyFilterOnTestIndexOf = function (pIndex) {
    return ($rootScope.radiologyFilterOnTest.findIndex(function (pData) {
      return (pData.tIndex == pIndex)
    }));
  }
  $scope.radiologyFilter = function (p) {
    if ($rootScope.radiologyFilterOnTestIndexOf(p.T) > -1) {
      return $scope.imagingAreaFilter.func(p);
    }
    return false;
  };
  // Click of Area buttons
  $scope.area_click = function (p) {
    $scope.areaDetailToShow = p;
  }
  $scope.areaDetailToShow = {
    "T": 0,
    "area": 0
  };
  // Select the first Area which is selected
  $scope.selectFirstArea = function () {
    $timeout(function () {
      $scope.areaDetailToShow = $rootScope.radiologyAreaList[$rootScope.radiologyAreaList.findIndex(function (p) {
        return p.sel
      })];
    }, 10);
  }
  // Get initial data
  $http.get("./labnbill/data/testData.json").then(function (response) {
    $scope.myLabBtnData = response.data[0].labBtnData;
  });
  // Click of NEXT button
  $scope.goNext = function () {
    if ($rootScope.doneable()) {
      if ($scope.ajaxCallFinished) {
        $rootScope.waitingPopup = true;
        $rootScope.redirect("/preview");
      } else {
        $scope.confimClicked = true;
      }
      return;
    }
    if ($rootScope.departments[$rootScope.radilogyUnitIndex.deptIndex].unitJ[$rootScope.radilogyUnitIndex.unitIndex].sel) {
      if ($scope.showExtra == "") {
        $scope.marginTop = 0;
        $scope.showExtra = "imagingTests";
      } else if ($scope.showExtra == "imagingTests") {
        $scope.showExtra = "imagingTestDetails";
        $scope.selectFirstArea();
      } else if ($scope.showExtra == "imagingTestDetails") {
        $scope.showExtra = "";
        $scope.shownDiv = "proc";
        $rootScope.radiologyFilterOnTest.map(function (pData) {
          pData.sel = true;
        });
        $scope.prepareRadiologyDataToSubmit();
      }
      $scope.marginTop = 0;
    } else {
      if ($scope.ajaxCallFinished) {
        $rootScope.redirect("/preview");
      } else {
        $scope.confimClicked = true;
      }
    }
  }
  // Click of BACK button
  $scope.goBack = function () {
    if ($rootScope.departments[$rootScope.radilogyUnitIndex.deptIndex].unitJ[$rootScope.radilogyUnitIndex.unitIndex].sel) {
      if ($scope.showExtra == "imagingTestDetails") {
        $scope.showExtra = "imagingTests";
      } else if ($scope.showExtra == "imagingTests") {
        $scope.showExtra = "";
      } else if ($scope.showExtra == "") {
        $rootScope.redirect("/main");
      }
      $scope.marginTop = 0;
    } else {
      $rootScope.redirect("/main");
    }
  }
  // Check if the Selection are complete and page is ready to be moved to next screen
  $rootScope.doneable = function () {
    for (var i = 0; i < $rootScope.radiologyFilterOnTest.length; i++) {
      if ($rootScope.radiologyFilterOnTest[i].sel == false) {
        return false;
      }
    }
    return true;
  }
  // Click of PROCEDURE buttons
  $rootScope.proc_click = function (pData) {
    if ($rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].clinicJ[pData.clinicIndex].isOrdered) {
      return;
    }
    $rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].clinicJ[pData.clinicIndex].sel = !$rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].clinicJ[pData.clinicIndex].sel;
    if ($rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].clinicJ.findIndex(function (pD) {
        return pD.sel == true
      }) > -1) {
      $rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].sel = true;
    } else {
      $rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].sel = false;
    }
    if ($rootScope.departments[pData.deptIndex].unitJ.findIndex(function (pD) {
        return pD.sel == true
      }) > -1) {
      $rootScope.departments[pData.deptIndex].sel = true;
    } else {
      $rootScope.departments[pData.deptIndex].sel = false;
    }
    $rootScope.preparePreview(pData.deptIndex, pData.unitIndex, pData.clinicIndex);
    if ($rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].clinicJ[pData.clinicIndex].radiologyDepartmentsIndex !== undefined) {
      if ($rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].clinicJ[pData.clinicIndex].sel) {
        $rootScope.radiologyFilterOnTest.push({
          "tIndex": $rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].clinicJ[pData.clinicIndex].radiologyDepartmentsIndex,
          "sel": false
        });
      } else {
        $rootScope.radiologyFilterOnTest.splice($rootScope.radiologyFilterOnTestIndexOf($rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].clinicJ[pData.clinicIndex].radiologyDepartmentsIndex), 1);
        for (var i = 0; i < $rootScope.radiologyAreaList.length; i++) {
          if ($rootScope.radiologyAreaList[i].T == pData.clinicIndex) {
            $rootScope.radiologyAreaList[i].sel = false;
            $rootScope.radiologyAreaList[i].selectedDetails = [];
            $rootScope.radiologyAreaList[i].details.medium = null;
            $rootScope.radiologyAreaList[i].details.position = null;
            $rootScope.radiologyAreaList[i].details.type = null;
            $rootScope.radiologyAreaList[i].details.view = null;
          }
        }
      }
    }
  };
  // Checks if there is any selection made or not
  $scope.isAnyTestPropSelected = function () {
    if ($rootScope.departments.findIndex(function (pD) {
        return pD.sel == true
      }) > -1) {
      return true;
    } else if ($rootScope.MyLab.findIndex(function (pl) {
        return pl.sel == true
      }) > -1) {
      return true;
    } else if ($rootScope.testList.length > 0) {
      return true;
    } else {
      return false;
    }
  }
  $scope.labPage = function () {
    $rootScope.redirect("/");
  }
  $scope.billPage = function () {
    $rootScope.redirect("/bill");
  }
  $scope.gotoPreview = function () {
    $rootScope.redirect("/preview");
  }
  $scope.labButtons = "lab";
  $scope.testProc = "test";
  $scope.ProcButtons = "proc";
  $scope.showExtra = "";
  $scope.shownDiv = "proc";
  // Filter for Procedure
  $scope.procFilter = {
    unit: [],
    alpha: "",
    number: "",
    func: function (p) {
      if ($scope.procFilter.unit.length == 0 && $scope.procFilter.alpha.length == 0 && $scope.procFilter.number.length == 0) {
        return true;
      } else {
        if ($scope.procFilter.unit.indexOf($rootScope.departments[p.deptIndex].unitJ[p.unitIndex].id) > -1) {
          if ($scope.procFilter.alpha.length > 0 || $scope.procFilter.number.length > 0) {
            return (($scope.procFilter.alpha.indexOf($rootScope.departments[p.deptIndex].unitJ[p.unitIndex].clinicJ[p.clinicIndex].name.substr(0, 1).toUpperCase()) > -1) || ($scope.procFilter.number.indexOf($rootScope.departments[p.deptIndex].unitJ[p.unitIndex].clinicJ[p.clinicIndex].name.substr(0, 1).toUpperCase()) > -1));
          } else {
            return true;
          }
        } else {
          if ($scope.procFilter.unit.length == 0) {
            return (($scope.procFilter.alpha.indexOf($rootScope.departments[p.deptIndex].unitJ[p.unitIndex].clinicJ[p.clinicIndex].name.substr(0, 1).toUpperCase()) > -1) || ($scope.procFilter.number.indexOf($rootScope.departments[p.deptIndex].unitJ[p.unitIndex].clinicJ[p.clinicIndex].name.substr(0, 1).toUpperCase()) > -1));
          }
        }
      }
      return false;
    }
  };
  // Filter for imaging Areas
  $scope.imagingAreaFilter = {
    dept: [],
    alpha: "",
    number: "",
    func: function (p) {
      if ($scope.imagingAreaFilter.dept.length == 0 && $scope.imagingAreaFilter.alpha.length == 0 && $scope.imagingAreaFilter.number.length == 0) {
        return true;
      } else {
        if ($scope.imagingAreaFilter.dept.indexOf(p.D) > -1) {
          return true;
        } else if ($scope.imagingAreaFilter.alpha.indexOf($rootScope.radiologyDepartments.testj[p.T].imageAreaJ[p.area].name.substr(0, 1).toUpperCase()) > -1) {
          return true
        } else if ($scope.imagingAreaFilter.number.indexOf($rootScope.radiologyDepartments.testj[p.T].imageAreaJ[p.area].name.substr(0, 1).toUpperCase()) > -1) {
          return true
        }
      }
      return false;
    }
  };
  // Check for filters
  $scope.filter_check = function (pData, pType) {
    if ($scope.shownDiv == "proc") {
      if ($scope.showExtra == "") {
        if (pType == "unit") {
          return ($scope.procFilter.unit.indexOf(pData) > -1);
        } else if (pType == "alpha") {
          return ($scope.procFilter.alpha.indexOf(pData) > -1);
        } else if (pType == "number") {
          return ($scope.procFilter.number.indexOf(pData) > -1);
        }
      } else if ($scope.showExtra == "imagingTests") {
        if (pType == "dept") {
          return ($scope.imagingAreaFilter.dept.indexOf(pData) > -1);
        } else if (pType == "alpha") {
          return ($scope.imagingAreaFilter.alpha.indexOf(pData) > -1);
        } else if (pType == "number") {
          return ($scope.imagingAreaFilter.number.indexOf(pData) > -1);
        }
      }
    }
  }
  // Click of all filter components
  $scope.filter_click = function (pData, pType) {
    if ($scope.shownDiv == "proc") {
      if ($scope.showExtra == "") {
        var foundAt = -1;
        if (pType == "unit") {
          if (pData == 10000) {
            for (var i = 0; i < $rootScope.departments.length - 1; i++) {
              for (var j = 0; j < $rootScope.departments[i].unitJ.length - 1; j++) {
                if (!$scope.deptsShownInLimits(i, j)) {
                  foundAt = $scope.procFilter.unit.indexOf($rootScope.departments[i].unitJ[j].id);
                  if (foundAt > -1) {
                    $scope.procFilter.unit.splice(foundAt, 1);
                  } else {
                    $scope.procFilter.unit.push($rootScope.departments[i].unitJ[j].id);
                  }
                }
              }
            }
          } else {
            foundAt = $scope.procFilter.unit.indexOf(pData);
            if (foundAt > -1) {
              $scope.procFilter.unit.splice(foundAt, 1);
            } else {
              $scope.procFilter.unit.push(pData);
            }
          }
        } else if (pType == "alpha") {
          foundAt = $scope.procFilter.alpha.indexOf(pData);
          if (foundAt > -1) {
            $scope.procFilter.alpha = $scope.procFilter.alpha.replace(pData, '');
          } else {
            $scope.procFilter.alpha += pData;
          }
        } else if (pType == "number") {
          foundAt = $scope.procFilter.number.indexOf(pData);
          if (foundAt > -1) {
            $scope.procFilter.number = $scope.procFilter.number.replace(pData, '');
          } else {
            $scope.procFilter.number += pData;
          }
        }
      } else if ($scope.showExtra == "imagingTests") {
        var foundAt = -1;
        if (pType == "dept") {} else if (pType == "alpha") {
          foundAt = $scope.imagingAreaFilter.alpha.indexOf(pData);
          if (foundAt > -1) {
            $scope.imagingAreaFilter.alpha = $scope.imagingAreaFilter.alpha.replace(pData, '');
          } else {
            $scope.imagingAreaFilter.alpha += pData;
          }
        } else if (pType == "number") {
          foundAt = $scope.imagingAreaFilter.number.indexOf(pData);
          if (foundAt > -1) {
            $scope.imagingAreaFilter.number = $scope.imagingAreaFilter.number.replace(pData, '');
          } else {
            $scope.imagingAreaFilter.number += pData;
          }
        }
      }
    }
    $scope.marginTop = 0;
  }
  $scope.marginTop = 0;
  $scope.marginLeft = 0;
  // Scroll button UP
  $scope.slideUp = function () {
    if (($scope.marginTop * -1) >= ($('.testFilterPanel').innerHeight() - 400)) {
      return
    }
    $scope.marginTop -= 400;
  }
  // Scroll button DOWN
  $scope.slideDown = function () {
    if (($scope.marginTop * -1) <= 0) {
      return
    }
    $scope.marginTop += 400;
  }
  // Scroll button LEFT
  $scope.slideLeft = function () {
    var scrollWidth1 = document.getElementById("shan").scrollWidth;
    if ((scrollWidth1 <= $('.scrollInside').innerWidth())) {
      return
    }
    $scope.marginLeft -= 100;
  }
  // Scroll button RIGHT
  $scope.slideRight = function () {
    if (($scope.marginLeft * -1) <= 0) {
      return
    }
    $scope.marginLeft += 100;
  }
  // Check for scroll limits
  $scope.isScrollUpperBound = function (pCssId, pScrollAt) {
    var scrollWidth1 = document.getElementById(pCssId).scrollWidth;
    if (scrollWidth1 <= 1368) {
      return false
    }
    if (((pScrollAt * -1) >= scrollWidth1 - 1368)) {
      return false
    }
    return true;
  }
  // Department filter for limits
  $scope.deptsShownInLimits = function (pDeptIndex, pIndex) {
    if ($rootScope.departments[pDeptIndex].unitJ[pIndex].patientLastVisitUnit == "yes" || $rootScope.departments[pDeptIndex].unitJ[pIndex].radiologyFlag !== undefined) {
      return true;
    }
    return false;
  }
  setTimeout(function () {
    var panelHeight = $('.testFilterPanel').innerHeight();
    var screenHeight = 540;
    var SliderNum = Math.floor(panelHeight / screenHeight);
    $('.testsFilterData').css("height", screenHeight);
    $('.testsFilterData').css("overflow", "hidden");
    var panelWidth = $('.scrollhor').innerWidth();
    $('.scrollhor').css("overflow", "hidden");
  }, 100);
  $scope.open = false;
  $scope.hideSpan = function (p) {
    $timeout(function () {
      if (document.getElementById("span" + p) != null) {
        document.getElementById("span" + p).remove();
      }
    }, 12000)
  }
  $scope.hideOthSpan = function () {
    $timeout(function () {
      if (document.getElementById("spanOth") != null) {
        document.getElementById("spanOth").remove();
      }
    }, 12000)
  }
  // Prepare preview after selections
  $rootScope.preparePreview = function (pDept, pUnit, pClinic) {
    if ($rootScope.departments[pDept].unitJ[pUnit].radiologyFlag) {
      return;
    }
    var unitFound = $rootScope.toPreviewUnitIndexOf(pDept, pUnit);
    var clinicFound = -1;
    if (unitFound < 0) {
      $rootScope.toPreview.push({
        deptIndex: pDept,
        unitIndex: pUnit,
        clinics: [{
          clinicIndex: pClinic,
          defaultRoom: null,
          defaultDate: "",
          availableDates: []
        }]
      });
      unitFound = $rootScope.toPreview.length - 1;
      if ($rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].defaultRoom === undefined) {
        var dataToSend = {
          "clinicId": $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].id
        };
        $http.post(getUrlIP() + "Spring/restServices/defaultRoomWithDate", dataToSend).then(function (response) {
          if (response.data == "") {} else {
            $rootScope.toPreview[unitFound].clinics[0].defaultDate = response.data.defaultAvlDate[0];
            $rootScope.toPreview[unitFound].clinics[0].availableDates = response.data.defaultAvlDate;
            $rootScope.toPreview[unitFound].clinics[0].defaultRoom = response.data.defaultRoomId;
            $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic]["defaultRoom"] = $rootScope.toPreview[unitFound].clinics[0].defaultRoom;
            for (var i = 0; i < $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].roomJ.length; i++) {
              if ($rootScope.toPreview[unitFound].clinics[0].defaultRoom == $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].roomJ[i].id) {
                $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].roomJ[i]["availableDates"] = $rootScope.toPreview[unitFound].clinics[0].availableDates;
                $rootScope.toPreview[unitFound].clinics[0].defaultRoom = i;
                $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].defaultRoom = i;
              }
            }
          }
        }, function (error) {});
      } else {
        $rootScope.toPreview[unitFound].clinics[0].defaultRoom = $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].defaultRoom;
        $rootScope.toPreview[unitFound].clinics[0].availableDates = $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].roomJ[$rootScope.toPreview[unitFound].clinics[0].defaultRoom].availableDates;
        $rootScope.toPreview[unitFound].clinics[0].defaultDate = $rootScope.toPreview[unitFound].clinics[0].availableDates[0];
      }
    } else {
      clinicFound = $rootScope.toPreviewClinicIndexOf(unitFound, pClinic);
      if (clinicFound < 0) {
        $rootScope.toPreview[unitFound].clinics.push({
          clinicIndex: pClinic,
          defaultRoom: null,
          defaultDate: "",
          availableDates: []
        });
        clinicFound = $rootScope.toPreview[unitFound].clinics.length - 1;
        if ($rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].defaultRoom === undefined) {
          var dataToSend = {
            "clinicId": $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].id
          };
          $http.post(getUrlIP() + "Spring/restServices/defaultRoomWithDate", dataToSend).then(function (response) {
            if (response.data == "") {} else {
              $rootScope.toPreview[unitFound].clinics[clinicFound].defaultDate = response.data.defaultAvlDate[0];
              $rootScope.toPreview[unitFound].clinics[clinicFound].availableDates = response.data.defaultAvlDate;
              $rootScope.toPreview[unitFound].clinics[clinicFound].defaultRoom = response.data.defaultRoomId;
              $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic]["defaultRoom"] = $rootScope.toPreview[unitFound].clinics[0].defaultRoom;
              for (var i = 0; i < $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].roomJ.length; i++) {
                if ($rootScope.toPreview[unitFound].clinics[clinicFound].defaultRoom == $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].roomJ[i].id) {
                  $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].roomJ[i]["availableDates"] = $rootScope.toPreview[unitFound].clinics[0].availableDates;
                  $rootScope.toPreview[unitFound].clinics[clinicFound].defaultRoom = i;
                  $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].defaultRoom = i;
                }
              }
            }
          }, function (error) {});
        } else {
          $rootScope.toPreview[unitFound].clinics[clinicFound].defaultRoom = $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].defaultRoom;
          $rootScope.toPreview[unitFound].clinics[clinicFound].availableDates = $rootScope.departments[pDept].unitJ[pUnit].clinicJ[pClinic].roomJ[$rootScope.toPreview[unitFound].clinics[clinicFound].defaultRoom].availableDates;
          $rootScope.toPreview[unitFound].clinics[clinicFound].defaultDate = $rootScope.toPreview[unitFound].clinics[clinicFound].availableDates[0];
        }
      } else {
        $rootScope.toPreview[unitFound].clinics.splice(clinicFound, 1);
      }
      if ($rootScope.toPreview[unitFound].clinics.length < 1) {
        $rootScope.toPreview.splice(unitFound, 1);
      }
    }
  }
  $rootScope.toPreviewUnitIndexOf = function (pDept, pUnit) {
    for (var i = 0; i < $rootScope.toPreview.length; i++) {
      if ($rootScope.toPreview[i].deptIndex == pDept && $rootScope.toPreview[i].unitIndex == pUnit) {
        return i;
      }
    }
    return -1;
  }
  $rootScope.toPreviewClinicIndexOf = function (pUnitIndexFoundInToPreview, pClinic) {
    for (var i = 0; i < $rootScope.toPreview[pUnitIndexFoundInToPreview].clinics.length; i++) {
      if ($rootScope.toPreview[pUnitIndexFoundInToPreview].clinics[i].clinicIndex == pClinic) {
        return i;
      }
    }
    return -1;
  }
  // Function to check default date present into "toPreview"
  $scope.isPreviewReady = function () {
    for (var i = 0; i < $rootScope.toPreview.length; i++) {
      for (var j = 0; j < $rootScope.toPreview[i].clinics.length; j++) {
        if ($rootScope.toPreview[i].clinics[j].defaultDate == null) {
          return false;
        }
      }
    }
    return true;
  };
  // Imaging area data calculation
  $scope.ImagingAreaDataCalc = function (pLength) {
    return (20 * Math.ceil(pLength / 3));
  }
  // Creates Imaging test names in proper format
  $scope.getImagingTestName = function (pData) {
    if (pData === null || pData === undefined) {
      return "";
    }
    var vName = "";
    vName += $rootScope.radiologyDepartments.testj[$scope.areaDetailToShow.T].testName;
    vName += " - " + $rootScope.radiologyDepartments.testj[$scope.areaDetailToShow.T].imageAreaJ[$scope.areaDetailToShow.area].name;
    if (pData.medium != null) {
      vName += " - " + pData.medium.name;
    }
    if (pData.type != null) {
      vName += " - " + pData.type.name;
    }
    if (pData.view != null) {
      vName += " - " + pData.view.name;
    }
    if (pData.position != null) {
      vName += " - " + pData.position.name;
    }
    return vName;
  }
  // Adds in Imaging Tests
  $rootScope.getImagingTestNameAdd = function (pData) {
    $rootScope.radiologyAreaList[$scope.areaDetailToShow.selfIndex].selectedDetails.push(angular.copy($rootScope.radiologyAreaList[$scope.areaDetailToShow.selfIndex].details));
    $rootScope.radiologyAreaList[$scope.areaDetailToShow.selfIndex].details.medium = null;
    $rootScope.radiologyAreaList[$scope.areaDetailToShow.selfIndex].details.position = null;
    $rootScope.radiologyAreaList[$scope.areaDetailToShow.selfIndex].details.type = null;
    $rootScope.radiologyAreaList[$scope.areaDetailToShow.selfIndex].details.view = null;
  }
  // Removes Imaging Test from selection
  $rootScope.getImagingTestNameBlank = function () {
    $rootScope.radiologyAreaList[$scope.areaDetailToShow.selfIndex].selectedDetails.splice($rootScope.radiologyAreaList[$scope.areaDetailToShow.selfIndex].selectedDetails.length - 1, 1);
  }
  // Prepare radiology data for submition
  $scope.prepareRadiologyDataToSubmit = function () {
    $scope.imagingDataToSubmit = {
      "categoryId": $rootScope.radiologyDepartments.categoryId,
      "categoryName": $rootScope.radiologyDepartments.categoryName,
      "tests": []
    };
    var testIndex = -1;
    for (var i = 0; i < $rootScope.radiologyAreaList.length; i++) {
      if ($rootScope.radiologyAreaList[i].sel == true) {
        testIndex = $scope.imagingDataToSubmitIndexOf($rootScope.radiologyAreaList[i].T);
        for (var j = 0; j < $rootScope.radiologyAreaList[i].selectedDetails.length; j++) {
          var areaToPush = {
            "imageArea": {
              "id": $rootScope.radiologyDepartments.testj[$rootScope.radiologyAreaList[i].T].imageAreaJ[$rootScope.radiologyAreaList[i].area].id,
              "name": $rootScope.radiologyDepartments.testj[$rootScope.radiologyAreaList[i].T].imageAreaJ[$rootScope.radiologyAreaList[i].area].name,
              "imageTypeJ": $rootScope.radiologyAreaList[i].selectedDetails[j].type,
              "imageViewJ": $rootScope.radiologyAreaList[i].selectedDetails[j].view,
              "imageMediumJ": $rootScope.radiologyAreaList[i].selectedDetails[j].medium,
              "imagePositionJ": $rootScope.radiologyAreaList[i].selectedDetails[j].position
            }
          };
          $scope.imagingDataToSubmit.tests[testIndex].imagingData.push(areaToPush);
        }
      }
    }
    var vFullDataToSubmit = {
      "retry": 0,
      "orderData": [],
      "isSubmited": false,
      "patientNotes": [],
      "doctorDetails": {
        "doctorID": $rootScope.loggedInpatientDetail.doc_id,
        "doctorName": $rootScope.loggedInpatientDetail.doc_name,
        "userName": null
      },
      "patientDetails": {
        /*"age": $rootScope.loggedInpatientDetail.patient_age,*/
        "age": "15Y2M5D",
        "sex": $rootScope.loggedInpatientDetail.gender,
        "dept": {
          "dept_id": $rootScope.loggedInpatientDetail.dept_id,
          "deptName": $rootScope.loggedInpatientDetail.dept_name
        },
        "name": $rootScope.loggedInpatientDetail.patient_name,
        "uhId": $rootScope.loggedInpatientDetail.uhId,
        "mobile": null,
        "appt_id": null,
        "pntdtls": null,
        "apptTime": null,
        "doctorId": $rootScope.loggedInpatientDetail.doc_id,
        "doctorName": $rootScope.loggedInpatientDetail.doc_name,
        "pntVisitDtls": null
      }
    };
    vFullDataToSubmit.orderData.push($scope.imagingDataToSubmit);
    $http.post(getUrlIP() + "Spring/restServices/radioTestToDefRoomAvlDateWO", vFullDataToSubmit).then(function (response) {
      if (response.data.errorCode !== undefined) {
        if (response.data.errorCode == "404") {}
      } else {
        $rootScope.radiologyOrderData = response.data;
        for (var i = 0; i < $rootScope.radiologyOrderData.length; i++) {
          $rootScope.radiologyOrderData[i]["ordered"] = false;
          for (var j = 0; j < $rootScope.radiologyOrderData[i].rooms.length; j++) {
            $rootScope.radiologyOrderData[i].rooms[j]["defaultDate"] = $rootScope.radiologyOrderData[i].rooms[j].date[0];
            for (var k = 0; k < $rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea.length; k++) {
              if ($rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea[k].constructor === Array) {
                $rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea[k] = $rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea[k][0];
              }
              $rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea[k]["active"] = true;
            }
          }
        }
      }
    }, function (error) {});
  }
  // Finds the Index of Object from imagingDataToSubmit based on input given
  $scope.imagingDataToSubmitIndexOf = function (pTestIndex) {
    for (var j = 0; j < $scope.imagingDataToSubmit.tests.length; j++) {
      if ($scope.imagingDataToSubmit.tests[j].id == $rootScope.radiologyDepartments.testj[pTestIndex].testId) {
        return j;
      }
    }
    $scope.imagingDataToSubmit.tests.push({
      "id": $rootScope.radiologyDepartments.testj[pTestIndex].testId,
      "name": $rootScope.radiologyDepartments.testj[pTestIndex].testName,
      "type": "imaging form",
      "preReq": [],
      "addlForm": null,
      "subTests": [],
      "labComment": "",
      "imagingData": [],
      "sampleTypes": []
    });
    return $scope.imagingDataToSubmit.tests.length - 1;
  }
  // On AJAX call finished
  $scope.$on('http:finished', function () {
    if ($scope.confimClicked) {
      $rootScope.waitingPopup = true;
      $rootScope.redirect("/preview");
    } else {
      $scope.ajaxCallFinished = true;
    }
  });
  // On AJAX call started and running
  $scope.$on('http:running', function () {
    $scope.ajaxCallFinished = false;
  });
  // checks Unit if already shown in the top left Department filter
  $scope.checkUnitIfPrevShown = function (pDept$index, pUnit) {
    for (var i = 0; i < pDept$index; i++) {
      for (var j = 0; j < $scope.departments[i].unitJ.length; j++) {
        if (pUnit.id == $scope.departments[i].unitJ[j].id) {
          if ($scope.departments[i].unitJ[j].patientLastVisitUnit == "yes") {
            return false;
          } else {
            return true;
          }
        }
      }
    }
    return true;
  };
});