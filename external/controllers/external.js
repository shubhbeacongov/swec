'use strict';
/**
 * @ngdoc function
 * @name kiosklabApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the kiosklabApp
 */
angular.module('swecApp').controller('externalCtrl', function ($rootScope, $sce, $scope) {
  $rootScope.extLink;
  // Link to be opened
  $scope.link = $sce.trustAsResourceUrl($rootScope.extLink);
  // Adjust iframe size to full
  function resizeIframe(obj) {
    obj.style.height = 0;
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
  // Navigate back
  $scope.goBack = function () {
    $rootScope.redirect("main");
  }
  $rootScope.waitingPopup = false;
});