'use strict';
angular.module('swecApp').controller('mainindexCtrl', function ($scope, $rootScope) {
  // Navigate to patient login page
  $rootScope.goPatientPage = function () {
    $rootScope.loggedInpatientDetail = null;
    $rootScope.redirect('/patientLogin');
  }
});