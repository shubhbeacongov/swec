angular.module("swecApp").service("MaterialCalendarData", [function () {
  function CalendarData() {
    this.data = {};
    this.getDayKey = function (date) {
      return [date.getFullYear(), date.getMonth() + 1, date.getDate()].join("-");
    };
    this.setDayContent = function (date, content) {
      this.data[this.getDayKey(date)] = content || this.data[this.getDayKey(date)] || "";
    };
  }
  return new CalendarData();
}]);