'use strict';
/**
 * @ngdoc function
 * @name kiosklabApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the kiosklabApp
 */
angular.module('swecApp').controller('patientLoginCtrl', function ($scope, $http, $rootScope, base64) {
  // Close waiting popup
  $rootScope.waitingPopup = false;
  // Initiating Panel Data
  $scope.mypanelData = null;
  // Fetch menu panel data
  $http.get("./appdata/homePanelData.json").then(function (response) {
    for (var i = 0; i < response.data[0].panelData.length; i++) {
      $scope.mypanelData = response.data[0].panelData;
    }
  });
  // Initiating message variables
  $scope.error_msg_english = "";
  $scope.error_msg_hindi = "";
  // Clearing error messages
  $scope.cleanError = function () {
    $scope.error_msg_english = "";
    $scope.error_msg_hindi = "";
  };
  // Auto fill inputs for ease
  $scope.fillInfo = function () {
    $scope.patient = {
      uhid: '100093691',
      mobile: '8273152161'
    };
  }
  // Patient Init
  $scope.patient = {
    uhid: '',
    mobile: ''
  };
  // Cursor position
  var startPos;
  // Set focused element for supporting virtual keyboard
  $scope.updateFocusElement = function () {
    $scope.fucusedItemName = angular.element(window.document.activeElement).attr('id');
    startPos = document.getElementById($scope.fucusedItemName).selectionStart;
  };
  // Set value from virtual keyboard to selected textboxes
  $scope.setValue = function (pvalue) {
    var divID = $scope.fucusedItemName;
    var loginvalue = document.getElementById(divID).value;
    var len = loginvalue.length;
    if (divID === 'UHID') {
      if (len > 10) {
        /*$scope.error_msg_english = "UHID Should Contain Only Eleven Digits";
        $scope.error_msg_hindi = "यू. एच. आई. डी. केवल ग्यारह अंक ही होना चाहिए";*/
        document.getElementById("UHID").focus();
        return;
      } else {
        document.getElementById(divID).value = document.getElementById(divID).value + pvalue;
        $scope.patient.uhid = document.getElementById(divID).value;
        startPos = startPos + 1;
        document.getElementById("UHID").focus();
      }
    } else {
      if (len > 9) {
        /*$scope.error_msg_english = "Mobile Number Should Contain Only Ten Digits";
        $scope.error_msg_hindi = "मोबाइल नंबर केवल दस अंक ही होना चाहिए";*/
        document.getElementById("MOB").focus();
        return;
      } else {
        document.getElementById(divID).value = document.getElementById(divID).value + pvalue;
        $scope.patient.mobile = document.getElementById(divID).value;
        startPos = startPos + 1;
        document.getElementById("MOB").focus();
      }
    }
  }
  // Clear Textbox Values
  $scope.clrValue = function () {
    var divID = $scope.fucusedItemName;
    var loginvalue = document.getElementById(divID).value;
    var loginvalue = document.getElementById(divID).value;
    var len = loginvalue.length;
    if (startPos == 0) {
      return;
    }
    if (divID === "UHID") {
      if (len > 0) {
        document.getElementById("UHID").focus();
        if (startPos < len) {
          var start = document.getElementById(divID).value.slice(0, startPos - 1);
          var end = document.getElementById(divID).value.slice(startPos, len);
          document.getElementById(divID).value = start + end;
          startPos = startPos - 1;
        } else {
          document.getElementById(divID).value = document.getElementById(divID).value.slice(0, -1);
          startPos = startPos - 1;
        }
      } else {
        document.getElementById(divID).value = document.getElementById(divID).value.slice(0, -1);
        startPos = startPos - 1;
      }
    } else {
      if (len > 0) {
        document.getElementById("MOB").focus();
        if (startPos < len) {
          var start = document.getElementById(divID).value.slice(0, startPos - 1);
          var end = document.getElementById(divID).value.slice(startPos, len);
          document.getElementById(divID).value = start + end;
          startPos = startPos - 1;
        } else {
          document.getElementById(divID).value = document.getElementById(divID).value.slice(0, -1);
          startPos = startPos - 1;
        }
      } else {
        document.getElementById(divID).value = document.getElementById(divID).value.slice(0, -1);
        startPos = startPos - 1;
      }
    }
  }
  // Clear all values
  $scope.clrValueAll = function () {
    var divID = $scope.fucusedItemName;
    document.getElementById(divID).value = "";
    document.getElementById(divID).focus();
  }
  $rootScope.waitingPopup = false;
  // Call server to get patient data
  $scope.getPatientData = function (patient) {
    var patientDatas = {
      'uhId': $scope.patient.uhid,
      'mobileNumber': $scope.patient.mobile
    };
    $http.defaults.headers.common['Authorization'] = 'Basic ' + base64.encode('username1' + ':' + 'password123');
    $rootScope.waitingPopup = true;
    $http.post(getUrlIP() + 'Spring/restServices/patientServiceWithIdMob', patientDatas).then(function (response) {
      $rootScope.waitingPopup = false;
      if (response.data.errorCode !== undefined) {
        if (response.data.errorCode === "404") {
          $scope.error_msg_english = "Patient Details Not Available";
          $scope.error_msg_hindi = "मरीज़ के विवरण उपलब्ध नहीं है";
          $scope.patient.uhid = "";
          $scope.patient.mobile = "";
          $rootScope.redirect("/patientLogin");
        }
        $rootScope.waitingPopup = false;
      } else {
        $rootScope.loggedInpatientDetail = response.data['diffgr:diffgram'].PatientDetails.PatientDetails[0];
        $rootScope.loggedInpatientDetail["uhId"] = $scope.patient.uhid;
        $rootScope.loggedInpatientDetail["mobile"] = $scope.patient.mobile;
        $rootScope.redirect("/main");
      }
      if (response.data.doctorDetails !== undefined) {
        $rootScope.alreadyOrdered = true;
        $rootScope.testList = response.data.orderData;
      }
    }, function (error) {
      if (error.data == null) {
        $scope.error_show = true;
        $scope.error_msg_english = "Check your internet connection";
        $scope.error_msg_hindi = "";
        $scope.patient.uhid = "";
        $scope.patient.mobile = "";
        $rootScope.redirect("/");
      }
      $rootScope.waitingPopup = false;
    });
    $rootScope.idealTimeIncrement = true;
    return;
  }
  // Validations
  $scope.validation = function () {
    if (($scope.patient.uhid === "" && $scope.patient.mobile === "") || ($scope.patient.uhid === undefined && $scope.patient.mobile === undefined) || ($scope.patient.uhid === undefined && $scope.patient.mobile === "") || ($scope.patient.uhid === "" && $scope.patient.mobile === undefined)) {
      $scope.error_msg_english = "Please Enter U.H.I.D. and Mobile Number";
      $scope.error_msg_hindi = "कृपया यू. एच. आई. डी. और मोबाइल नंबर दर्ज करे";
      return false;
    } else if (($scope.patient.uhid === "" && $scope.patient.mobile !== "") || ($scope.patient.uhid !== "" && $scope.patient.mobile === "") || ($scope.patient.uhid === undefined && $scope.patient.mobile !== "") || ($scope.patient.uhid !== "" && $scope.patient.mobile === undefined)) {
      $scope.error_msg_english = "Please Enter Valid U.H.I.D. and Mobile Number";
      $scope.error_msg_hindi = "कृपया सही  यू. एच. आई. डी. और मोबाइल नंबर दर्ज करे";
      return false;
    } else if ($scope.patient.uhid !== "" && $scope.patient.mobile !== "") {
      if ($scope.patient.uhid.length < 9 && $scope.patient.mobile.length === 10) {
        $scope.error_msg_english = "Please Enter Valid U.H.I.D.";
        $scope.error_msg_hindi = "कृपया सही यू. एच. आई. डी. दर्ज करे";
        return false;
      } else if ($scope.patient.mobile.length !== 10 && $scope.patient.uhid.length > 8) {
        $scope.error_msg_english = "Please Enter Valid Mobile Number";
        $scope.error_msg_hindi = "कृपया सही मोबाइल नंबर दर्ज करे";
        return false;
      } else if ($scope.patient.uhid.length < 9 && $scope.patient.mobile.length !== 10) {
        $scope.error_msg_english = "Please Enter Valid LogiId and Password";
        $scope.error_msg_hindi = "कृपया सही लॉग इन आई. डी. और पासवर्ड दर्ज करे";
        return false;
      } else {
        return true;
      }
    }
  };
  // Click of OK button
  $scope.patientLogin1 = function (patient) {
    if ($scope.validation()) {
      var data = $scope.getPatientData(patient);
      return data;
    } else {
      $scope.validation();
    }
  }
});