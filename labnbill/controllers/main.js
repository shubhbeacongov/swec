'use strict';
angular.module('swecApp').controller('MainCtrl', function ($scope, $http, $rootScope, base64) {
  // Initiating Panel Data
  $scope.mypanelData = null;
  // Fetch menu panel data
  $http.get("./appdata/homePanelData.json").then(function (response) {
    for (var i = 0; i < response.data[0].panelData.length; i++) {
      $scope.mypanelData = response.data[0].panelData;
    }
    $rootScope.waitingPopup = false;
  });
  // External link URL
  $rootScope.extLink = "";
  // Navigate to external link
  $rootScope.externalLink = function (route, link) {
    $rootScope.waitingPopup = true;
    $rootScope.refreshData = true;
    $rootScope.extLink = link;
    $rootScope.redirect(route);
  };
});