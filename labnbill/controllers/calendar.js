'use strict';
/**
 * @ngdoc function
 * @name kiosklabApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the kiosklabApp
 */
angular.module('swecApp').controller('calendarCtrl', function ($scope) {
  // Default Date
  $scope.myDate = new Date();
  // Minimum Date out of range
  $scope.minDate = new Date($scope.myDate.getFullYear(), $scope.myDate.getMonth(), $scope.myDate.getDate());
  // Maximum Date out of range
  $scope.maxDate = new Date($scope.myDate.getFullYear(), $scope.myDate.getMonth() + 2, $scope.myDate.getDate());
  // Weekday Check
  $scope.onlyWeekendsPredicate = function (date) {
    var day = date.getDay();
    return day === 0 || day === 6;
  };
});