angular.module('swecApp').controller('billCtrl', function ($scope, $http, $rootScope) {
  this.awesomeThings = ['HTML5 Boilerplate', 'AngularJS', 'Karma'];
  // Remove Procedure data from bill page
  $scope.removeProc = function () {
    $rootScope.proc_click({
      "clinicIndex": $rootScope.previewDataForBill.data.toClinicPreview.clinicIndex,
      "deptIndex": $rootScope.previewDataForBill.data.toUnitPreview.deptIndex,
      "unitIndex": $rootScope.previewDataForBill.data.toUnitPreview.unitIndex
    });
    $scope.goPreview();
  }
  // Remove Radiology Data on Bill page
  $scope.removeRadiologyProc = function (pDataIndex) {
    $rootScope.previewDataForBill.data.imagingRoomToPreview.procedures[0].imagingArea.splice(pDataIndex, 1);
    if ($rootScope.previewDataForBill.data.imagingRoomToPreview.procedures[0].imagingArea.length < 1) {
      for (var i = 0; i < $rootScope.radiologyOrderData.length; i++) {
        if ($rootScope.previewDataForBill.data.imagingTestName == $rootScope.radiologyOrderData[i].imagingType) {
          for (var j = 0; j < $rootScope.radiologyOrderData[i].rooms.length; j++) {
            if ($rootScope.previewDataForBill.data.imagingRoomToPreview.roomNumber == $rootScope.radiologyOrderData[i].rooms[j].roomNumber) {
              $rootScope.radiologyOrderData[i].rooms.splice(j, 1);
              break;
            }
          }
          if ($rootScope.radiologyOrderData[i].rooms.length < 1) {
            $rootScope.removeRadiologyTests($rootScope.radiologyOrderData[i].imagingType);
          }
        }
      }
    }
  }
  // Navigate to Preview
  $scope.goPreview = function () {
    $rootScope.redirect("/preview");
  }
  // Scroll Button
  $scope.slideDownButtonLimitBill = function (pScrollAt) {
    if ((pScrollAt * -1) + 300 < $('#previewId').innerHeight()) {
      return true;
    }
    return false;
  }
});