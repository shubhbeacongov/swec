angular.module("swecApp").directive("calendarMd", ["$compile", "$parse", "$templateRequest", "$q", "materialCalendar.Calendar", "MaterialCalendarData", function ($compile, $parse, $templateRequest, $q, Calendar, CalendarData, $scope) {
  var defaultTemplate = "<md-content layout='column' layout-fill md-swipe-left='next()' md-swipe-right='prev()' style='background-color: transparent;'><md-toolbar style='background-color: transparent;'><div class='md-toolbar-tools' layout='row'><md-button class='md-icon-button' ng-click='prev()' aria-label='Previous month'><md-tooltip ng-if='::tooltips()'>Previous month</md-tooltip><md-icon class='glyphicon glyphicon-menu-left' style='color: white;font-size: 30px;'></md-icon></md-button><div flex></div><h1 class='calendar-md-title'><span style='font-size:30px'>{{ calendar.start | date:titleFormat:timezone }}</span></h1><div flex></div><md-button class='md-icon-button' ng-click='next()' aria-label='Next month'><md-tooltip ng-if='::tooltips()'>Next month</md-tooltip><md-icon class='moveNext glyphicon glyphicon-menu-left' style='color: white;font-size: 30px;'></md-icon></md-button></div></md-toolbar><!-- agenda view --><md-content ng-if='weekLayout === columnWeekLayout' class='agenda'><div ng-repeat='week in calendar.weeks track by $index'><div ng-if='sameMonth(day)' ng-class='{&quot;disabled&quot; : isDisabled(day), active: active === day }' ng-click='handleDayClick(day)' ng-repeat='day in week' layout><md-tooltip ng-if='::tooltips()'>{{ day | date:dayTooltipFormat:timezone }}</md-tooltip><div>{{ day | date:dayFormat:timezone }}</div><div flex compile='dataService.data[dayKey(day)]' ></div></div></div></md-content><!-- calendar view --><md-content ng-if='weekLayout !== columnWeekLayout' flex layout='column' class='calendar' style='background-color:transparent; border:0;'><div layout='row' class='subheader' style='color: #78cfe7;font-size: 20px;padding: 5px 0; border:0'><div layout-padding class='subheader-day' flex ng-repeat='day in calendar.weeks[0]'><md-tooltip ng-if='::tooltips()'>{{ day | date:dayLabelTooltipFormat }}</md-tooltip>{{ day | date:dayLabelFormat }}</div></div><div ng-if='week.length' ng-repeat='week in calendar.weeks track by $index' flex layout='row'><div tabindex='{{ sameMonth(day) ? (day | date:dayFormat:timezone) : 0 }}' ng-repeat='day in week track by $index' ng-click='handleDayClick(day)' flex layout layout-padding  ng-focus='focus = true;' ng-blur='focus = false;' style='outline:0'><md-tooltip ng-if='::tooltips()'>{{ day | date:dayTooltipFormat }}</md-tooltip><div class='dateText' ng-class='getDateClass(day)'>{{ day | date:dayFormat }}</div><div flex compile='dataService.data[dayKey(day)]' id='{{ day | date:dayIdFormat }}' style='display:none'></div></div></div></md-content></md-content>";
  var injectCss = function () {
    var styleId = "calendarMdCss";
    if (!document.getElementById(styleId)) {
      var head = document.getElementsByTagName("head")[0];
      var css = document.createElement("style");
      css.type = "text/css";
      css.id = styleId;
      css.innerHTML = "calendar-md md-content>md-content.agenda>*>* :not(:first-child),calendar-md md-content>md-content.calendar>:not(:first-child)>* :last-child{overflow:hidden;text-overflow:ellipsis}calendar-md{display:block;max-height:100%;height:100%;width:100%}calendar-md .md-toolbar-tools h2{overflow-x:hidden;text-overflow:ellipsis;white-space:nowrap}calendar-md .md-toolbar-tools .moveNext{transform:translate3d(0,0,0) rotate(180deg)}calendar-md md-content>md-content{border:1px solid rgba(0,0,0,.12)}calendar-md md-content>md-content.agenda>*>*{border-bottom:1px solid rgba(0,0,0,.12)}calendar-md md-content>md-content.agenda>*>.disabled{color:rgba(0,0,0,.3);pointer-events:none;cursor:auto}calendar-md md-content>md-content.agenda>*>* :first-child{padding:12px;width:200px;text-align:right;color:rgba(0,0,0,.75);font-weight:100;overflow-x:hidden;text-overflow:ellipsis;white-space:nowrap}calendar-md md-content>md-content>*>*{min-width:48px}calendar-md md-content>md-content.calendar>:first-child{background:rgba(0,0,0,.02);border-bottom:1px solid rgba(0,0,0,.12);margin-right:0;min-height:36px}calendar-md md-content>md-content.calendar>:not(:first-child)>*{cursor:pointer}calendar-md md-content>md-content.calendar>:not(:first-child)>:hover{}calendar-md md-content>md-content.calendar>:not(:first-child)>.disabled{color:rgba(0,0,0,.3);pointer-events:none;cursor:auto}calendar-md md-content>md-content.calendar>:not(:first-child)>.active{}calendar-md md-content>md-content.calendar>:not(:first-child)>* :first-child{padding:0}";
      head.insertBefore(css, head.firstChild);
    }
  };
  return {
    restrict: "E",
    scope: {
      ngModel: "=?",
      template: "&",
      templateUrl: "=?",
      onDayClick: "=?",
      onPrevMonth: "=?",
      onNextMonth: "=?",
      calendarDirection: "=?",
      dayContent: "&?",
      timezone: "=?",
      titleFormat: "=?",
      dayFormat: "=?",
      dayLabelFormat: "=?",
      dayLabelTooltipFormat: "=?",
      dayTooltipFormat: "=?",
      weekStartsOn: "=?",
      tooltips: "&?",
      startDateOfMonth: "=?",
      noOfDays: "=?",
      clearDataCacheOnLoad: "=?",
      disableFutureSelection: "=?",
      disableSelection: "=?",
      availableDates: "=?"
    },
    link: function ($scope, $element, $attrs) {
      // Add the CSS here.
      injectCss();
      var date = new Date();
      var month = parseInt($attrs.startMonth || date.getMonth());
      var year = parseInt($attrs.startYear || date.getFullYear());
      $scope.columnWeekLayout = "column";
      $scope.weekLayout = "row";
      $scope.timezone = $scope.timezone || null;
      $scope.noCache = $attrs.clearDataCacheOnLoad || false;
      // Parse the parent model to determine if it's an array.
      // If it is an array, than we'll automatically be able to select
      // more than one date.
      if ($attrs.ngModel) {
        $scope.active = $scope.$parent.$eval($attrs.ngModel);
        if ($attrs.ngModel) {
          $scope.$watch("$parent." + $attrs.ngModel, function (val) {
            $scope.active = val;
          });
        }
      } else {
        $scope.active = null;
      }
      // Set the defaults here.
      $scope.titleFormat = $scope.titleFormat || "LLLL yyyy";
      $scope.dayLabelFormat = $scope.dayLabelFormat || "EEE";
      $scope.dayLabelTooltipFormat = $scope.dayLabelTooltipFormat || "EEEE";
      $scope.dayFormat = $scope.dayFormat || "d";
      $scope.dayTooltipFormat = $scope.dayTooltipFormat || "fullDate";
      $scope.dayIdFormat = "dd-MM-yy";
      $scope.disableFutureSelection = $scope.disableFutureSelection || false;
      $scope.disableSelection = $scope.disableSelection || false;
      $scope.sameMonth = function (date) {
        var d = angular.copy(date);
        return d.getFullYear() === $scope.calendar.year && d.getMonth() === $scope.calendar.month;
      };
      $scope.isDisabled = function (date, startDateOfMonth, noOfDays) {
        if (noOfDays != 0 && angular.isDefined(noOfDays)) {
          var dateStart = new Date($scope.calendar.year, $scope.calendar.month, startDateOfMonth);
          var dateEnd = angular.copy(dateStart);
          dateEnd.setDate(dateStart.getDate() + parseInt(noOfDays));
          if (date.getDate() <= dateStart && date.getDate() >= dateEnd) {
            return true;
          }
        }
        if ($scope.disableSelection) {
          return true;
        }
        if ($scope.disableFutureSelection && date > new Date()) {
          return true;
        }
        return !$scope.sameMonth(date);
      };
      $scope.calendarDirection = $scope.calendarDirection || "horizontal";
      $scope.$watch("calendarDirection", function (val) {
        $scope.weekLayout = val === "horizontal" ? "row" : "column";
      });
      $scope.$watch("weekLayout", function () {
        year = $scope.calendar.year;
        month = $scope.calendar.month;
        bootstrap();
      });
      var handleCb = function (cb, data) {
        (cb || angular.noop)(data);
      };
      var dateFind = function (arr, date) {
        var index = -1;
        angular.forEach(arr, function (d, k) {
          if (index < 0) {
            if (angular.equals(date, d)) {
              index = k;
            }
          }
        });
        return index;
      };
      $scope.isBeforeToday = function (date) {
        var currDate = new Date();
        currDate.setHours(0, 0, 0, 0);
        if (date < (currDate)) {
          return true;
        }
        return false;
      };
      $scope.isToday = function (date) {
        var vToday = new Date();
        if (angular.equals(date.getYear(), vToday.getYear()) && angular.equals(date.getMonth(), vToday.getMonth()) && angular.equals(date.getDate(), vToday.getDate())) {
          return true;
        }
        return false;
      };
      $scope.isActive = function (date) {
        var dDate = date.getDate();
        var dMonth = date.getMonth() + 1 + "";
        if (dMonth.length < 2) {
          dMonth = "0" + dMonth;
        }
        var dYear = date.getFullYear();
        var properDate = dDate + "-" + dMonth + "-" + dYear;
        var match;
        var active = angular.copy($scope.active);
        if (!angular.isArray(active)) {
          if (active == properDate) {
            match = true;
          }
        } else {
          match = dateFind(active, date) > -1;
        }
        return match;
      };
      $scope.isBusy = function (date) {
        var vDate = date.getDate() + "";
        if (vDate.length < 2) {
          vDate = "0" + vDate;
        }
        var vMonth = date.getMonth() + 1 + "";
        if (vMonth.length < 2) {
          vMonth = "0" + vMonth;
        }
        var vYear = date.getFullYear();
        var properDate = vDate + "-" + vMonth + "-" + vYear;
        for (var i = 0; i < $scope.availableDates.length; i++) {
          if (properDate == $scope.availableDates[i]) {
            return false;
          }
        }
        return true;
      };
      $scope.getDateClass = function (date) {
        if ($scope.isDisabled(date)) {
          return "disabledDate";
        } else if ($scope.isBeforeToday(date)) {
          return "disabledDate";
        } else if ($scope.isBusy(date)) {
          return "busyDate";
        } else if ($scope.isActive(date)) {
          return "selectedDate";
        } else if ($scope.isToday(date)) {
          return "todayDate";
        }
      };
      $scope.prev = function () {
        $scope.calendar.prev();
        var data = {
          year: $scope.calendar.year,
          month: $scope.calendar.month + 1
        };
        setData();
        handleCb($scope.onPrevMonth, data);
      };
      $scope.next = function () {
        $scope.calendar.next();
        var data = {
          year: $scope.calendar.year,
          month: $scope.calendar.month + 1
        };
        setData();
        handleCb($scope.onNextMonth, data);
      };
      $scope.handleDayClick = function (date) {
        if ($scope.isDisabled(date)) {
          return;
        }
        if ($scope.isBeforeToday(date)) {
          return;
        }
        if ($scope.isBusy(date)) {
          return;
        }
        if ($scope.disableFutureSelection && date > new Date()) {
          return;
        }
        if ($scope.disableSelection) {
          return;
        }
        var active = angular.copy($scope.active);
        if (angular.isArray(active)) {
          var idx = dateFind(active, date);
          if (idx > -1) {
            active.splice(idx, 1);
          } else {
            active.push(date);
          }
        } else {
          var vDate = date.getDate();
          var vMonth = date.getMonth() + 1 + "";
          if (vMonth.length < 2) {
            vMonth = "0" + vMonth;
          }
          var vYear = date.getFullYear();
          var properDate = vDate + "-" + vMonth + "-" + vYear;
          if (angular.equals(active, properDate)) {
            active = properDate;
          } else {
            active = properDate;
          }
        }
        $scope.active = active;
        if ($attrs.ngModel) {
          $parse($attrs.ngModel).assign($scope.$parent, angular.copy($scope.active));
        }
        handleCb($scope.onDayClick, angular.copy(date));
      };
      // Small helper function to set the contents of the template.
      var setTemplate = function (contents) {
        $element.html(contents);
        $compile($element.contents())($scope);
      };
      var init = function () {
        $scope.calendar = new Calendar(year, month, {
          weekStartsOn: $scope.weekStartsOn || 0,
          startDateOfMonth: $scope.startDateOfMonth || 1,
          noOfDays: $scope.noOfDays || 0
        });
        // Allows fetching of dynamic templates via $templateCache.
        if ($scope.templateUrl) {
          return $templateRequest($scope.templateUrl);
        }
        return $q.resolve($scope.template() || defaultTemplate);
      };
      $scope.dataService = CalendarData;
      // Set the html contents of each date.
      var getDayKey = function (date) {
        return $scope.dataService.getDayKey(date);
      };
      $scope.dayKey = getDayKey;
      var getDayContent = function (date) {
        // Initialize the data in the data array.
        if ($scope.noCache) {
          $scope.dataService.setDayContent(date, "");
        } else {
          $scope.dataService.setDayContent(date, ($scope.dataService.data[getDayKey(date)] || ""));
        }
        var cb = ($scope.dayContent || angular.noop)();
        var result = (cb || angular.noop)(date);
        // Check for async function. This should support $http.get() and also regular $q.defer() functions.
        if (angular.isObject(result) && "function" === typeof result.success) {
          result.success(function (html) {
            $scope.dataService.setDayContent(date, html);
          });
        } else if (angular.isObject(result) && "function" === typeof result.then) {
          result.then(function (html) {
            $scope.dataService.setDayContent(date, html);
          });
        } else {
          $scope.dataService.setDayContent(date, result);
        }
      };
      var setData = function () {
        angular.forEach($scope.calendar.weeks, function (week) {
          angular.forEach(week, getDayContent);
        });
      };
      window.data = $scope.data;
      var bootstrap = function () {
        init().then(function (contents) {
          setTemplate(contents);
          setData();
        });
      };
      $scope.$watchGroup(["weekStartsOn", "startDateOfMonth", "noOfDays"], init);
      bootstrap();
      // These are for tests, don't remove them..
      $scope._$$init = init;
      $scope._$$setTemplate = setTemplate;
      $scope._$$bootstrap = bootstrap;
    }
  };
}]);