'use strict';
angular.module('swecApp').controller('userLoginCtrl', function ($scope, $http, $rootScope, base64, $document) {
  // Initiating message variables
  $scope.error_msg_english = "";
  $scope.error_msg_hindi = "";
  // Clearing error messages
  $scope.cleanError = function () {
    $scope.error_msg_english = "";
    $scope.error_msg_hindi = "";
  };
  // Auto fill inputs for ease
  $scope.fillInfo = function () {
    $scope.user = {
      loginId: '9871646597',
      password: '1234'
    };
  }
  // User Init   
  $scope.user = {
    loginId: '',
    password: ''
  };
  // called On Init
  $scope.onFocus = function () {
    document.getElementById('loginid').focus();
  }
  // Cursor position
  var startPos;
  // Set focused element for supporting virtual keyboard
  $scope.updateFocusElement = function () {
    $scope.fucusedItemName = angular.element(window.document.activeElement).attr('id');
    startPos = document.getElementById($scope.fucusedItemName).selectionStart;
  };
  // Set value from virtual keyboard to selected textboxes
  $scope.setValue = function (pvalue) {
    var divID = $scope.fucusedItemName;
    var loginvalue = document.getElementById(divID).value;
    var len = loginvalue.length;
    if (divID === 'loginid') {
      if (len > 9) {
        /*$scope.error_msg_english = "Please Enter Valid LogiId";
                    $scope.error_msg_hindi = "कृपया सही लॉग इन आई. डी. दर्ज करे"; */
        return;
      } else {
        document.getElementById(divID).value = document.getElementById(divID).value + pvalue;
        $scope.user.loginId = document.getElementById(divID).value;
        startPos = startPos + 1;
        document.getElementById("loginid").focus();
      }
    } else {
      if (len > 3) {
        /* $scope.error_msg_english = "Please Enter Valid Password";
        $scope.error_msg_hindi = "कृपया सही पासवर्ड दर्ज करे";*/
        return;
      } else {
        document.getElementById(divID).value = document.getElementById(divID).value + pvalue;
        $scope.user.password = document.getElementById(divID).value;
        startPos = startPos + 1;
        document.getElementById("userpassword").focus();
      }
    }
  }
  // Clear Textbox Values
  $scope.clrValue = function () {
    var divID = $scope.fucusedItemName;
    var loginvalue = document.getElementById(divID).value;
    var len = loginvalue.length;
    if (startPos == 0) {
      return;
    }
    var len = document.getElementById(divID).value.length;
    if (len > 0) {
      if (startPos < len) {
        var start = document.getElementById(divID).value.slice(0, startPos - 1);
        var end = document.getElementById(divID).value.slice(startPos, len);
        document.getElementById(divID).value = start + end;
        startPos = startPos - 1;
      } else {
        document.getElementById(divID).value = document.getElementById(divID).value.slice(0, -1);
        startPos = startPos - 1;
      }
    } else {
      document.getElementById(divID).value = document.getElementById(divID).value.slice(0, -1);
      startPos = startPos - 1;
    }
  }
  // Clear all values
  $scope.clrValueAll = function () {
    var divID = $scope.fucusedItemName;
    document.getElementById(divID).value = "";
    $scope.cleanError();
    document.getElementById(divID).focus();
  }
  $rootScope.waitingPopup = false;
  // Call server to get user data
  $scope.getLogin = function (user) {
    var loginDet = {
      'username': $scope.user.loginId,
      'password': $scope.user.password
    };
    $http.defaults.headers.common['Authorization'] = 'Basic ' + base64.encode('username1' + ':' + 'password123');
    $rootScope.waitingPopup = true;
    $http.post(getUrlIP() + 'Spring/restServices/userAuth', loginDet).then(function (response) {
      if (response.data.errorCode !== undefined) {
        if (response.data.errorCode == "404") {
          $scope.error_show = true;
          $scope.error_msg_english = "Invalid User";
          $scope.error_msg_hindi = "";
          $scope.user.loginId = "";
          $scope.user.password = "";
          $rootScope.redirect("/");
        } else if (response.data.errorCode == "500") {
          $scope.error_show = true;
          $scope.error_msg_english = "Could not open connection";
          $scope.error_msg_hindi = "";
          $scope.user.loginId = "";
          $scope.user.password = "";
        }
        $rootScope.waitingPopup = false;
      } else {
        $rootScope.redirect('/patientLogin');
        $rootScope.waitingPopup = false;
      }
    }, function (error) {
      if (error.data == null) {
        $scope.error_show = true;
        $scope.error_msg_english = "Check your internet connection";
        $scope.error_msg_hindi = "";
        $scope.user.loginId = "";
        $scope.user.password = "";
        $rootScope.redirect("/");
      }
      $rootScope.waitingPopup = false;
    });
    return;
  }
  // Validations
  $scope.validation = function () {
    if ($scope.user.loginId === "" && $scope.user.password === "") {
      $scope.error_msg_english = "Please Enter LogiId and Password Number";
      $scope.error_msg_hindi = "कृपया लॉग इन आई. डी. और पासवर्ड दर्ज करे";
      return false;
    } else if ($scope.user.loginId.length !== 10 && $scope.user.password.length === 0) {
      $scope.error_msg_english = "Please Enter Valid LogiId and Password";
      $scope.error_msg_hindi = "कृपया सही लॉग इन आई. डी. और पासवर्ड दर्ज करे";
      return false;
    } else if ($scope.user.loginId.length === 0 && $scope.user.password.length !== 4) {
      $scope.error_msg_english = "Please Enter Valid LogiId and Password";
      $scope.error_msg_hindi = "कृपया सही लॉग इन आई. डी. और पासवर्ड दर्ज करे";
      return false;
    } else if ($scope.user.loginId !== "" && $scope.user.password !== "") {
      if ($scope.user.loginId.length !== 10 && $scope.user.password.length === 4) {
        $scope.error_msg_english = "Please Enter Valid LogiId";
        $scope.error_msg_hindi = "कृपया सही लॉग इन आई. डी. दर्ज करे";
        return false;
      } else if ($scope.user.password.length !== 4 && $scope.user.loginId.length === 10) {
        $scope.error_msg_english = "Please Enter Valid Password";
        $scope.error_msg_hindi = "कृपया सही पासवर्ड दर्ज करे";
        return false;
      } else if ($scope.user.loginId.length !== 10 && $scope.user.password.length !== 4) {
        $scope.error_msg_english = "Please Enter Valid LogiId and Password";
        $scope.error_msg_hindi = "कृपया सही लॉग इन आई. डी. और पासवर्ड दर्ज करे";
        return false;
      } else {
        return true;
      }
    }
  };
  // Click of OK button
  $scope.login = function (user) {
    if ($scope.validation()) {
      var data = $scope.getLogin(user);
      return data;
    } else {
      $scope.validation();
    }
  }
});