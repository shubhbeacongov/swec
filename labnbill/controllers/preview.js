'use strict';
/**
 * @ngdoc function
 * @name kiosklabApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the kiosklabApp
 */
angular.module('swecApp').controller('previewCtrl', function ($scope, $http, $rootScope) {
  $rootScope.deptRoom = {
    D: -1,
    R: -1
  };
  $scope.goTest = function () {
    $rootScope.redirect("/test");
  }
  // Redirects to bill page with preview selected data
  $scope.goToBill = function (pFor, pData) {
    $rootScope.previewDataForBill = {
      "for": pFor,
      "data": pData
    }
    $rootScope.redirect("bill");
  }
  $rootScope.previewDataForBill = {};
  // Deletes preview card
  $scope.removeDept = function (pData) {
    while (pData.clinics.length > 0) {
      $rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].clinicJ[pData.clinics[0].clinicIndex].sel = false;
      $rootScope.preparePreview(pData.deptIndex, pData.unitIndex, pData.clinics[0].clinicIndex);
    }
    $rootScope.departments[pData.deptIndex].unitJ[pData.unitIndex].sel = false;
    for (var j = 0; j < $rootScope.departments[pData.deptIndex].unitJ.length; j++) {
      if ($rootScope.departments[pData.deptIndex].unitJ[j].sel = true) {
        return;
      }
    }
    $rootScope.departments[pData.deptIndex].sel = false;
  }
  // Scroll Button
  $scope.slideDownButtonLimit = function (pScrollAt) {
    var parentScrollWidth = $('#shanId')[0].scrollWidth;
    if ((pScrollAt * -1) + 1140 < parentScrollWidth) {
      return true;
    }
    return false;
  }
  //----Calendar Code starts here----------------------------------------------------------------
  $scope.dayFormat = "d";
  // To select a single date, make sure the ngModel is not an array.
  $scope.selectedDate = new Date();
  // If you want multi-date select, initialize it as an array.
  $scope.firstDayOfWeek = 0; // First day of the week, 0 for Sunday, 1 for Monday, etc.
  $scope.setDirection = function (direction) {
    $scope.direction = direction;
    $scope.dayFormat = direction === "vertical" ? "EEEE, MMMM d" : "d";
  };
  $scope.dayClick = function (date) {
    $scope.msg = "You clicked " + $filter("date")(date, "MMM d, y h:mm:ss a Z");
  };
  $scope.prevMonth = function (data) {
    $scope.msg = "You clicked (prev) month " + data.month + ", " + data.year;
  };
  $scope.nextMonth = function (data) {
    $scope.msg = "You clicked (next) month " + data.month + ", " + data.year;
  };
  $scope.tooltips = true;
  $scope.setDayContent = function (date) {
    // You would inject any HTML you wanted for
    // that particular date here.
    return "<p></p>";
    // You could also use an $http function directly.
    return $http.get("/some/external/api");
    // You could also use a promise.
    var deferred = $q.defer();
    $timeout(function () {
      deferred.resolve("<p></p>");
    }, 1000);
    return deferred.promise;
  };
  // Calender Position
  $scope.calenderPopupPosition = {
    "top": null,
    "left": null,
    "rightShown": false
  };
  $scope.calenderDate_click = function (ev) {
    $scope.calenderPopupPosition.top = angular.element(ev.target).prop('offsetTop');
    $scope.calenderPopupPosition.left = angular.element(ev.target).prop('offsetLeft');
    if ($scope.calenderPopupPosition.left > 1100) {
      $scope.calenderPopupPosition.left = $scope.calenderPopupPosition.left - 680;
      $scope.calenderPopupPosition.rightShown = true;
    } else {
      $scope.calenderPopupPosition.left = $scope.calenderPopupPosition.left + 100;
      $scope.calenderPopupPosition.rightShown = false;
    }
  }
  // Available Dates for a Room
  $scope.getRoomAvailableDates = function (pDept, pClinic) {
    if ($rootScope.departments[pDept.deptIndex].unitJ[pDept.unitIndex].clinicJ[pClinic.clinicIndex].roomJ[pClinic.defaultRoom].availableDates === undefined) {
      var dataToSend = {
        "uhId": $rootScope.loggedInpatientDetail.uhId,
        "deptId": $rootScope.departments[pDept.deptIndex].id,
        "clinicId": $rootScope.departments[pDept.deptIndex].unitJ[pDept.unitIndex].id,
        "roomId": $rootScope.departments[pDept.deptIndex].unitJ[pDept.unitIndex].clinicJ[pClinic.clinicIndex].roomJ[pClinic.defaultRoom].id
      };
      $rootScope.waitingCalenderPopup = true;
      $http.post(getUrlIP() + "Spring/restServices/nxtAvlDate", dataToSend).then(function (response) {
        var data = response.data["diffgr:diffgram"].DoctorDetails.DoctorDetails
        var avlDates = [];
        var uniqueArray;
        try {
          for (var avldate = 0; avldate < data.length; avldate++) {
            var alldates = data[avldate].appointment_date.split("-").reverse().join("-");
            avlDates.push(alldates);
            uniqueArray = avlDates.filter(function (item, pos) {
              return avlDates.indexOf(item) == pos;
            });
          }
        } catch (ex) {}
        $rootScope.departments[pDept.deptIndex].unitJ[pDept.unitIndex].clinicJ[pClinic.clinicIndex].roomJ[pClinic.defaultRoom].availableDates = uniqueArray;
        $rootScope.departments[pDept.deptIndex].unitJ[pDept.unitIndex].clinicJ[pClinic.clinicIndex].roomJ[pClinic.defaultRoom].defaultDate = uniqueArray[0];
        $rootScope.waitingCalenderPopup = false;
      }, function (error) {});
    }
  }
  // Preview Card Default Date  Right arrow for selcting next date
  $scope.getNextDate = function (pDefaultDate, pAvailableDateArray) {
    for (var i = 0; i < pAvailableDateArray.length - 1; i++) {
      if (pDefaultDate == pAvailableDateArray[i]) {
        return pAvailableDateArray[i + 1];
      }
    }
    return pDefaultDate;
  }
  //Preview Card Default Date Left arrow for selcting previous date
  $scope.getPrevDate = function (pPDefaultDate, pPAvailableDateArray) {
    for (var i = 1; i < pPAvailableDateArray.length; i++) {
      if (pPDefaultDate == pPAvailableDateArray[i]) {
        return pPAvailableDateArray[i - 1];
      }
    }
    return pPDefaultDate;
  }
  // Displaying Radiology Procedure name on Radiology Preview Card
  $scope.getRadioActualProcNames = function (pProcs) {
    var vName = " - ";
    for (var i = 0; i < pProcs.length; i++) {
      vName += pProcs[i].procedureName;
    }
    return vName;
  }
  // Displaying Imaging Area Name on radiology Preview card
  $rootScope.getImagingtestName = function (pArea) {
    if (pArea.constructor === Array) {
      pArea = pArea[0];
    }
    var vName = pArea.name;
    if (pArea.imageMediumJ[0] != null) {
      vName += " - " + pArea.imageMediumJ[0].name;
    }
    if (pArea.imagePositionJ[0] != null) {
      vName += " - " + pArea.imagePositionJ[0].name;
    }
    if (pArea.imageTypeJ[0] != null) {
      vName += " - " + pArea.imageTypeJ[0].name;
    }
    if (pArea.imageViewJ[0] != null) {
      vName += " - " + pArea.imageViewJ[0].name;
    }
    return vName;
  }
  // Print Slip
  $scope.getPrintSlip = function (pDeptName, pApptDate, pPatientName, pSex, pAge, pMobile, pUhId, pTestName, pRoomName) {
    var out = "";
    out += `<center> <div id="printableArea" style="padding:0 20px">  
                    
                    <div style="text-align:center;">
                        <div style="display:flex; border-bottom:1px solid #000;">
                            <div style="width: 60% !important;">
                                <p><strong>ALL INDIA INSTITUTE OF MEDICAL SCIENCE (AIIMS)</strong></p>
                                <p>New Delhi,</p>
                                <p><strong><u>APPOINTMENT SLIP</u></strong></p>
                            </div>
                            <div style="width: 20% !important;bottom:0">
                                <img src="./barcode.png">
                            </div>
                        </div>
                        <div style="padding:10px;">
                            <p style="float:left;"><strong>Department Name:   ` + pDeptName + `  </strong></p>    
                        </div>
                        
                    </div>
                    <center> 
                    <table style="margin-top:2px; border: 1px solid grey; border-collapse: collapse; width:100%"> 
                    <tr> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;">Appointment &nbsp No.:</p></td> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;"><strong>  201702032514   </strong></p></td>  
                    
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;">Appointment &nbsp Date:</p></td> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;"><strong>  ` + pApptDate + `   </strong></p></td>  
                    
                    </tr> 
                    <tr> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;">Patient &nbsp UHID:</p></td> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;"><strong>  ` + pUhId + `  </strong></p></td>  
                    
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;">Name&nbsp of &nbsp Patient:</p></td> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;"><strong>  ` + pPatientName + `  </strong></p></td>  
                    </tr> 
                    <tr> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;">Age:</p></td> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;"><strong>  ` + pAge + `  </strong></p></td>  

                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;">Sex:</p></td> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;"><strong>  ` + pSex + `  </strong></p></td> 
                    </tr>
                    <tr> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;">Observation Type:</p></td> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;"><strong>  ` + pTestName + `  </strong></p></td>  

                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;">Location:</p></td> 
                    <td style="padding:0 2px; text-align: left; border: 1px solid grey; border-collapse: collapse;"><p style="font-size:20px; margin:0;"><strong>  ` + pRoomName + `  </strong></p></td> 
                    </tr> 
                    
                    </table> 
                    </center> 
                    <br/>
                    <!--span style="font-size:18px; margin:0;"><strong>Remarks: </strong></span-->
                    <br/>
                    <!--span style="font-size:13px; margin-left:20px;"><strong>Your UHID is : ` + pUhId + `</strong></span--> 
                    </div> </center>`;
    // Popup receipt in iframe
    function Popup(data) {
      var frame1 = document.createElement('iframe');
      frame1.name = "frame1";
      var cssLink = document.createElement("link")
      frame1.style.position = "absolute";
      frame1.style.top = "-1000px";
      frame1.style.width = "250px";
      frame1.style.height = "280px";
      document.body.appendChild(frame1);
      var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
      frameDoc.document.open();
      frameDoc.document.write('<html><head><title></title>');
      frameDoc.document.write('</head><link rel="stylesheet" href="css/style.css"><body>');
      frameDoc.document.write(data);
      frameDoc.document.write('</body></html>');
      frameDoc.document.write('</body></html>');
      frameDoc.document.close();
      setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
      }, 500);
      return false;
    }
    Popup(out);
  }
  // Remove Radiology Preview card
  $rootScope.removeRadiologyTests = function (pTestName) {
    for (var i = 0; i < $rootScope.radiologyOrderData.length; i++) {
      if ($rootScope.radiologyOrderData[i].imagingType == pTestName) {
        $rootScope.radiologyOrderData.splice(i, 1);
      }
    }
    for (var i = 0; i < $rootScope.departments[$rootScope.radilogyUnitIndex.deptIndex].unitJ[$rootScope.radilogyUnitIndex.unitIndex].clinicJ.length; i++) {
      if ($rootScope.departments[$rootScope.radilogyUnitIndex.deptIndex].unitJ[$rootScope.radilogyUnitIndex.unitIndex].clinicJ[i].name == pTestName) {
        $rootScope.proc_click({
          "clinicIndex": i,
          "deptIndex": $rootScope.radilogyUnitIndex.deptIndex,
          "unitIndex": $rootScope.radilogyUnitIndex.unitIndex
        });
      }
    }
  }
  // Remove from preview order & from test
  $scope.removeFromPreviewOrder = function (pIndex) {
    $rootScope.previewOrder.splice(pIndex, 1);
  }
  // Check if the card requires bigger height
  $scope.bigHeightRequired = function (pDomId) {
    var vScrollHeight = $('#' + pDomId)[0].scrollHeight;
    if (vScrollHeight > 380) {
      return true;
    }
    return false;
  }
  // Click of CONFIRM button
  $scope.confirm = function () {
    $scope.confirmPrinting();
  }
  // Preparing Confirm Order
  $scope.prepareConfirmOrder = function () {
    //------------TEST--------------------------------------------------------------------
    var testArr = angular.copy($rootScope.testList);
    for (var i = 0; i < testArr.length; i++) {
      for (var j = 0; j < testArr[i].rooms.length; j++) {
        delete testArr[i].rooms[j].date;
      }
    }
    //------------RADIOLOGY--------------------------------------------------------------------
    var radiologyArr = angular.copy($rootScope.radiologyOrderData);
    for (var i = 0; i < radiologyArr.length; i++) {
      for (var j = 0; j < radiologyArr[i].rooms.length; j++) {
        delete radiologyArr[i].rooms[j].date;
      }
    }
    //------------LAB--------------------------------------------------------------------
    var labArr = angular.copy($rootScope.MyLab.filter(function (p) {
      return p.sel
    }));
    for (var i = 0; i < labArr.length; i++) {
      labArr[i].unitJ = labArr[i].unitJ.filter(function (p) {
        return p.sel
      });
      delete labArr[i].sel;
      for (var j = 0; j < labArr[i].unitJ.length; j++) {
        labArr[i].unitJ[j].clinicJ = labArr[i].unitJ[j].clinicJ.filter(function (p) {
          return p.sel
        });
        delete labArr[i].unitJ[j].sel;
        delete labArr[i].unitJ[j].patientLastVisitUnit;
        for (var k = 0; k < labArr[i].unitJ[j].clinicJ.length; k++) {
          labArr[i].unitJ[j].clinicJ[k]["selectedRoom"] = labArr[i].unitJ[j].clinicJ[k].roomJ[labArr[i].unitJ[j].clinicJ[k].defaultRoom];
          //--Removing extra Data
          delete labArr[i].unitJ[j].clinicJ[k].sel;
          delete labArr[i].unitJ[j].clinicJ[k].defaultRoom;
          delete labArr[i].unitJ[j].clinicJ[k].roomJ;
          delete labArr[i].unitJ[j].clinicJ[k].selectedRoom.appointmentDate;
          delete labArr[i].unitJ[j].clinicJ[k].selectedRoom.sel;
          delete labArr[i].unitJ[j].clinicJ[k].selectedRoom.procedureJ;
        }
      }
    }
    //------------PROC--------------------------------------------------------------------
    for (var i = 0; i < $rootScope.toPreview.length; i++) {
      for (var j = 0; j < $rootScope.toPreview[i].clinics.length; j++) {
        var defRoom = $rootScope.departments[$rootScope.toPreview[i].deptIndex].unitJ[$rootScope.toPreview[i].unitIndex].clinicJ[$rootScope.toPreview[i].clinics[j].clinicIndex].defaultRoom;
        $rootScope.departments[$rootScope.toPreview[i].deptIndex].unitJ[$rootScope.toPreview[i].unitIndex].clinicJ[$rootScope.toPreview[i].clinics[j].clinicIndex]["selectedRoom"] = $rootScope.departments[$rootScope.toPreview[i].deptIndex].unitJ[$rootScope.toPreview[i].unitIndex].clinicJ[$rootScope.toPreview[i].clinics[j].clinicIndex].roomJ[defRoom];
        $rootScope.departments[$rootScope.toPreview[i].deptIndex].unitJ[$rootScope.toPreview[i].unitIndex].clinicJ[$rootScope.toPreview[i].clinics[j].clinicIndex].selectedRoom["selectedDate"] = $rootScope.toPreview[i].clinics[j].defaultDate;
      }
    }
    var procArr = angular.copy($rootScope.departments.filter(function (p) {
      return (p.sel && p.id != 500)
    }));
    for (var i = 0; i < procArr.length; i++) {
      procArr[i].unitJ = procArr[i].unitJ.filter(function (p) {
        return p.sel
      });
      delete procArr[i].sel;
      for (var j = 0; j < procArr[i].unitJ.length; j++) {
        delete procArr[i].unitJ[j].sel;
        delete procArr[i].unitJ[j].color;
        delete procArr[i].unitJ[j].bordercolor;
        delete procArr[i].unitJ[j].spancolor;
        delete procArr[i].unitJ[j].patientLastVisitUnit;
        delete procArr[i].unitJ[j].labButtonColor;
        procArr[i].unitJ[j].clinicJ = procArr[i].unitJ[j].clinicJ.filter(function (p) {
          return p.sel
        });
        for (var k = 0; k < procArr[i].unitJ[j].clinicJ.length; k++) {
          delete procArr[i].unitJ[j].clinicJ[k].roomJ;
          delete procArr[i].unitJ[j].clinicJ[k].sel;
          delete procArr[i].unitJ[j].clinicJ[k].defaultRoom;
          delete procArr[i].unitJ[j].clinicJ[k].selectedRoom.procedureJ;
          delete procArr[i].unitJ[j].clinicJ[k].selectedRoom.docProcJ;
          delete procArr[i].unitJ[j].clinicJ[k].selectedRoom.appointmentDate;
          delete procArr[i].unitJ[j].clinicJ[k].selectedRoom.availableDates;
        }
      }
    }
    var orderToSubmit = {
      "patientDetails": $rootScope.loggedInpatientDetail,
      "tests": testArr,
      "radiology": radiologyArr,
      "lab": labArr,
      "procedures": procArr
    }
    $http.post("http://tcs1:8080/Spring/restServices/swecApptSubmit", orderToSubmit).then(function (response) {
      if (response.data.errorCode !== undefined) {
        if (response.data.errorCode == "404") {
          alert("oops network error");
        }
      } else {
        if (response.data > 0) {
          $scope.confirmPrinting();
          $rootScope.redirect("patientLogin");
        } else {}
      }
    }, function (error) {});
  }
  // PrintSlip Data for patient
  $scope.confirmPrinting = function () {
    var vDeptName = "";
    var vApptDate = "";
    var vTestName = "";
    var vRoomName = "";
    var vPatientName = $rootScope.loggedInpatientDetail.patient_name;
    var vSex = $rootScope.loggedInpatientDetail.gender;
    var vAge = $rootScope.loggedInpatientDetail.patient_age;
    var vMobile = $rootScope.loggedInpatientDetail.mobile;
    var vUhId = $rootScope.loggedInpatientDetail.uhId;
    var orderToSubmit = [];
    //------------TEST--------------------------------------------------------------------
    for (var i = 0; i < $rootScope.testList.length; i++) {
      for (var j = 0; j < $rootScope.testList[i].rooms.length; j++) {
        for (var k = 0; k < $rootScope.testList[i].rooms[j].test.length; k++) {
          vDeptName = $rootScope.testList[i].collecCntrName;
          vApptDate = $rootScope.testList[i].rooms[j].defaultDate;
          vTestName = $rootScope.testList[i].rooms[j].test[k].name;
          vRoomName = $rootScope.testList[i].rooms[j].room;
          $scope.getPrintSlip(vDeptName, vApptDate, vPatientName, vSex, vAge, vMobile, vUhId, vTestName, vRoomName);
        }
      }
    }
    //------------RADIOLOGY--------------------------------------------------------------------
    for (var i = 0; i < $rootScope.radiologyOrderData.length; i++) {
      for (var j = 0; j < $rootScope.radiologyOrderData[i].rooms.length; j++) {
        for (var k = 0; k < $rootScope.radiologyOrderData[i].rooms[j].procedures.length; k++) {
          vDeptName = "Radiology";
          vApptDate = $rootScope.radiologyOrderData[i].rooms[j].defaultDate;
          vTestName = $scope.getImagingtestName($rootScope.radiologyOrderData[i].rooms[j].procedures[k].imagingArea);
          vRoomName = $rootScope.radiologyOrderData[i].rooms[j].roomNumber;
          $scope.getPrintSlip(vDeptName, vApptDate, vPatientName, vSex, vAge, vMobile, vUhId, vTestName, vRoomName);
        }
      }
    }
    //------------LAB--------------------------------------------------------------------
    for (var i = 0; i < $rootScope.MyLab.length; i++) {
      if ($rootScope.MyLab[i].sel == false) continue;
      for (var j = 0; j < $rootScope.MyLab[i].unitJ.length; j++) {
        if ($rootScope.MyLab[i].unitJ[j].sel == false) continue;
        for (var k = 0; k < $rootScope.MyLab[i].unitJ[j].clinicJ.length; k++) {
          if ($rootScope.MyLab[i].unitJ[j].clinicJ[k].sel == false) continue;
          vDeptName = $rootScope.MyLab[i].unitJ[j].clinicJ[k].name;
          vApptDate = $rootScope.MyLab[i].unitJ[j].clinicJ[k].roomJ[$rootScope.MyLab[i].unitJ[j].clinicJ[k].defaultRoom].defaultDate;
          vTestName = "";
          vRoomName = $rootScope.MyLab[i].unitJ[j].clinicJ[k].roomJ[$rootScope.MyLab[i].unitJ[j].clinicJ[k].defaultRoom].name;
          $scope.getPrintSlip(vDeptName, vApptDate, vPatientName, vSex, vAge, vMobile, vUhId, vTestName, vRoomName);
        }
      }
    }
    //------------PROC--------------------------------------------------------------------
    for (var i = 0; i < $rootScope.toPreview.length; i++) {
      for (var j = 0; j < $rootScope.toPreview[i].clinics.length; j++) {
        vDeptName = $rootScope.departments[$rootScope.toPreview[i].deptIndex].unitJ[$rootScope.toPreview[i].unitIndex].unitName;
        vApptDate = $rootScope.toPreview[i].clinics[j].defaultDate;
        vTestName = $rootScope.departments[$rootScope.toPreview[i].deptIndex].unitJ[$rootScope.toPreview[i].unitIndex].clinicJ[$rootScope.toPreview[i].clinics[j].clinicIndex].name;
        vRoomName = $rootScope.departments[$rootScope.toPreview[i].deptIndex].unitJ[$rootScope.toPreview[i].unitIndex].clinicJ[$rootScope.toPreview[i].clinics[j].clinicIndex].roomJ[$rootScope.toPreview[i].clinics[j].defaultRoom].name;
        $scope.getPrintSlip(vDeptName, vApptDate, vPatientName, vSex, vAge, vMobile, vUhId, vTestName, vRoomName);
      }
    }
    $scope.refreshForNewPatient();
  }
  // Prepare Preview Card For Radiology, Lab, Procedure
  $scope.preparePreviewOrder = function () {
    $rootScope.previewOrder = [];
    //------------TEST--------------------------------------------------------------------
    for (var i = 0; i < $rootScope.testList.length; i++) {
      var lineCount = 0;
      for (var j = 0; j < $rootScope.testList[i].rooms.length; j++) {
        lineCount++;
        if ($rootScope.testList[i].rooms[j].test.length >= 2) {
          lineCount = lineCount + 2;
        } else {
          lineCount++;
        }
      }
      $rootScope.previewOrder.push({
        "type": "test",
        "index": i,
        "date": $rootScope.testList[i].rooms[0].defaultDate,
        "height": lineCount
      });
    }
    //------------RADIOLOGY--------------------------------------------------------------------
    for (var i = 0; i < $rootScope.radiologyOrderData.length; i++) {
      var lineCount = 0;
      for (var j = 0; j < $rootScope.radiologyOrderData[i].rooms.length; j++) {
        lineCount++;
        if ($rootScope.radiologyOrderData[i].rooms[j].procedures[0].imagingArea.length >= 2) {
          lineCount = lineCount + 2;
        } else {
          lineCount++;
        }
      }
      $rootScope.previewOrder.push({
        "type": "radiology",
        "index": i,
        "date": $rootScope.radiologyOrderData[i].rooms[0].defaultDate,
        "height": lineCount
      });
    }
    //------------LAB--------------------------------------------------------------------
    for (var i = 0; i < $rootScope.labIndexes.length; i++) {
      if ($rootScope.MyLab[$rootScope.labIndexes[i].labDeptIndex].unitJ[$rootScope.labIndexes[i].labUnitIndex].clinicJ[$rootScope.labIndexes[i].labClinicIndex].sel) {
        $rootScope.previewOrder.push({
          "type": "lab",
          "index": i,
          "date": $rootScope.MyLab[$rootScope.labIndexes[i].labDeptIndex].unitJ[$rootScope.labIndexes[i].labUnitIndex].clinicJ[$rootScope.labIndexes[i].labClinicIndex].roomJ[$rootScope.MyLab[$rootScope.labIndexes[i].labDeptIndex].unitJ[$rootScope.labIndexes[i].labUnitIndex].clinicJ[$rootScope.labIndexes[i].labClinicIndex].defaultRoom].defaultDate,
          "height": 1
        });
      }
    }
    //------------PROC--------------------------------------------------------------------
    for (var i = 0; i < $rootScope.toPreview.length; i++) {
      var lineCount = $rootScope.toPreview[i].clinics.length * 2;
      $rootScope.previewOrder.push({
        "type": "proc",
        "index": i,
        "date": $rootScope.toPreview[i].clinics[0].defaultDate,
        "height": lineCount
      });
    }
    //------------Date Sorting Here--------------------------------------------------------------
    $rootScope.previewOrder.sort(function (date1, date2) {
      var arrDate1 = date1.date.split("-");
      var arrDate2 = date2.date.split("-");
      var vDate1 = new Date(arrDate1[1] + "/" + arrDate1[0] + "/" + arrDate1[2]);
      var vDate2 = new Date(arrDate2[1] + "/" + arrDate2[0] + "/" + arrDate2[2]);
      return vDate1 - vDate2;
    });
    //------------Height Sorting Here--------------------------------------------------------------
    for (var i = 0; i < $rootScope.previewOrder.length; i++) {
      if ($rootScope.previewOrder[i].height <= 6) {
        if ($rootScope.previewOrder[i + 1] !== undefined && $rootScope.previewOrder[i].date == $rootScope.previewOrder[i + 1].date) {
          if ($rootScope.previewOrder[i + 1].height <= 6) {
            i++;
          } else {
            for (var j = i + 2; j < $rootScope.previewOrder.length; j++) {
              if ($rootScope.previewOrder[i].date == $rootScope.previewOrder[j].date && $rootScope.previewOrder[j].height <= 6) {
                $rootScope.previewOrder.splice(i + 1, 0, $rootScope.previewOrder[j]);
                $rootScope.previewOrder.splice(j + 1, 1);
                i++;
                break;
              }
            }
          }
        }
      }
    }
  }
  $scope.preparePreviewOrder();
  // Delete RootScope Variables------
  $scope.refreshForNewPatient = function () {
    delete $rootScope.departments;
    delete $rootScope.procRepList;
    delete $rootScope.buttonColor;
    delete $rootScope.toPreview;
    delete $rootScope.radiologyDepartments;
    delete $rootScope.labIndexes;
    delete $rootScope.MyLab;
    delete $rootScope.radilogyUnitIndex;
    $rootScope.loggedInpatientDetail = null;
    delete $rootScope.radiologyDepartments;
    delete $rootScope.radiologyAreaList;
    $rootScope.waitingPopup = true;
    delete $rootScope.radiologyOrderData;
    delete $rootScope.radiologyFilterOnTest;
    delete $rootScope.testList;
    delete $rootScope.testIndexList;
    delete $rootScope.alreadyOrdered;
    $rootScope.refreshData = true;
    delete $rootScope.toPreview;
    delete $rootScope.deptRoom;
    delete $rootScope.toUnitPreview;
    delete $rootScope.toClinicPreview;
    delete $rootScope.imagingTestIndexToShowFor;
    delete $rootScope.toDept;
    delete $rootScope.toRoom;
    delete $rootScope.imagingTestName;
    delete $rootScope.imagingRoomName;
    delete $rootScope.billList;
    delete $rootScope.previewOrder;
    delete $rootScope.extLink;
    $rootScope.goPatientPage();
  }
  $rootScope.waitingPopup = false;
});